#pragma once
#include "Object.h"
class Enemy_Slash_Object :
	public Object
{
public:
	Enemy_Slash_Object(VEC2 vec);
	Enemy_Slash_Object(float x, float y);
	~Enemy_Slash_Object();

	bool Update() override;

private:
	int countdown;
};