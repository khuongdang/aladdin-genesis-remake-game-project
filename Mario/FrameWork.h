#pragma once
#include "System.h"
#include "DXManager.h"
#include "KeyboardManager.h"

class FrameWork
{
public:
	FrameWork(HINSTANCE* hInstance, int nCmdShow);
	~FrameWork();
	bool Init();
	bool Run();
	bool End();
private:

	WNDCLASSEX m_wc;
	int m_nCmdShow;
	HWND m_hWnd;

	HINSTANCE m_hInstance;
};

