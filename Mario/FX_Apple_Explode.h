#pragma once
#include "Object.h"
class FX_Apple_Explode :
	public Object
{
public:
	FX_Apple_Explode(VEC2 pos);
	~FX_Apple_Explode();
	bool Update();
	bool Init_Sprite();
};

