#pragma once
#include "GroundEnemy.h"
#include "Player.h"
class Nahbi :
	public GroundEnemy
{
public:
	Nahbi(int X, int Y, Player* Following = nullptr);
	~Nahbi();


	bool Update() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Slash();

	bool Stand();

private:

	Player* m_Following;
	bool Init_Sprite() override;
};

