#pragma once
#include "Projectile.h"
class Apple :
	public Projectile
{
public:

	Apple(int x, int y);
	bool Update() override;
	int Hit();

	~Apple();

private:
	bool Init_Sprite() override;
	
};

