#pragma once

#include "System.h"

#include "Sprite.h"
#include "Node.h"

class QuadTree;

class Object
{
public:
	Object();
	virtual ~Object() {};

	QuadTree* m_parent = nullptr;
	int m_index = 0;

	LPWSTR m_TypeName = L"Object";
	int m_bit_Mask;
	int m_colision_Mask;
	
	virtual bool Update() { return 0; };
	bool Render();
	virtual bool Move() { return 0; };
	virtual bool Stand() { return 0; };
	virtual bool Down() { return 0; };
	virtual bool Change_State(int State,bool lock = false);

	VEC2 set_Position(float x, float y);
	VEC2 get_Position();
	VEC2 set_Gravity(float x, float y);
	VEC2 set_Gravity(VEC2 vec);
	bool Traslate(float x, float y);
	bool Translate(VEC2 vec);
	VEC2 set_Accelerator(VEC2 vec);
	VEC2 set_Accelerator(float x, float y);
	VEC2 get_Accelerator();

	bool set_Velocity(VEC2 vec);
	bool set_Velocity(float x, float y);

	VEC2 get_Velocity();

	bool set_bodySize(VEC2 vec);
	bool set_bodySize(int x, int y);

	bool set_anchorPoint(VEC2 vec);
	bool set_anchorPoint(int x, int y);

	bool set_Static(bool param);
	bool get_isStatic();

	int get_facing();
	
	int virtual Hit() { return 0; };

	virtual RECT get_Rect() { return m_rigid_body; };

protected:
	virtual bool Init_Sprite() { return 0; };

	Sprite m_sprite = Sprite();
	Node m_node;

	RECT m_rigid_body;

	VEC2 m_body_size;
	VEC2 m_anchor_point = VEC2(0,0);

	bool m_isStatic = true;

	bool m_Blocking_Sprite = false;

	RECT Rect_Update();

	int m_state;
	int m_facing;

	bool is_destroying = false;
	bool m_actacked = false;

	int m_immute = 0;

	std::vector <Sprite*> State_Sprite;
};