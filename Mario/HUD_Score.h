#pragma once

#include "System.h"

#include "HUD_Desert_Number.h"
#include "HUD_Number.h"

class HUD_Score
{
public:
	HUD_Score();
	~HUD_Score();
	bool Render(VEC2 vec, int amount);

	bool Render(float x, float y, int amount);

	HUD_Desert_Number* display;
};

