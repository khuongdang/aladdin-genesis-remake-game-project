#include "Coal.h"
#include "FXmanager.h"


Coal::Coal(float x, float y, float width, float height)
{
	m_TypeName = L"COAL";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	m_body_size = VEC2(width, height);

	m_rigid_body = Rect_Update();

	m_isStatic = true;

	countdown = 60;
}


Coal::~Coal()
{
}

bool Coal::Update()
{
	if (countdown > 0)
		countdown--;
	return true;
}
  
bool Coal::Create_Fire(VEC2 pos)
{
	if (countdown == 0)
	{
		countdown = 30;
		VEC2 position = pos;
		pos.y = m_node.get_Position().y;
		FXmanager::get_instance()->CreateFX(L"Fire_Coal",pos);
		return true;
	}
	return false;
}


