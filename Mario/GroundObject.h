#pragma once

#include "Object.h"

class GroundObject : public Object
{
public:
	GroundObject();
	~GroundObject();
	bool is_Grounded();
	bool set_Grounded(bool param);
	virtual bool Jump() { return 0; };
	virtual bool Move_Left() { return 0; };
	virtual bool Move_Right() { return 0; };

protected:
	bool m_isGrounded;
};

