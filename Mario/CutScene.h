#pragma once
#include "System.h"
#include "Sprite.h"

using namespace std;

class CutScene
{
public:
	CutScene(LPDIRECT3DDEVICE9* d3ddv, LPDIRECT3DSURFACE9* back_buffer, LPDIRECT3DSURFACE9* surface);
	~CutScene();

	bool is_Active();
	bool Init();
	bool New(LPWSTR name);
	bool Run();
	bool is_end();
	bool End();

	static CutScene* get_Instance();

private:
	CutScene();

	LPDIRECT3DDEVICE9* m_d3ddv = nullptr;
	LPDIRECT3DSURFACE9* m_back_buffer = nullptr;
	LPDIRECT3DSURFACE9* m_surface = nullptr;

	LPWSTR state;

	vector<Sprite> S_array = vector<Sprite>(0);

	bool m_is_active;
	bool m_is_done;

	long countdown;

	LPD3DXSPRITE m_spriteHandler;

	static CutScene m_Instance;
};

