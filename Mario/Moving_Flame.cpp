#include "Moving_Flame.h"
#include "QuadTree.h"
#include "Sound.h"


Moving_Flame::Moving_Flame(VEC2 vec)
{
	m_TypeName = L"MOVING_FLAME";

	m_node = Node(vec);

	Init_Sprite();


	m_state = STATE_IDLE;

	m_body_size = VEC2(50, 20);
	m_rigid_body = Rect_Update();
	Sound::GetInstance()->Play("Fire From Coal");
}


Moving_Flame::~Moving_Flame()
{
}

bool Moving_Flame::Update()
{
	if (m_node.get_Velocity_X()>= 0)
		m_facing = FACE_RIGHT;
	else
		m_facing = FACE_LEFT;

	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

int Moving_Flame::Hit()
{
	return m_parent->Destroy(this);
}

bool Moving_Flame::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Projectile\\Moving_Flame.png", 101, 53, 8, 8, 3);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
