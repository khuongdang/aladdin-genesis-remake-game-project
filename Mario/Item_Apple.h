#pragma once
#include "Object.h"

class Item_Apple : public Object
{
public:
	Item_Apple(float X, float Y);
	bool Init_Sprite();
	int Hit();
	~Item_Apple();
};

