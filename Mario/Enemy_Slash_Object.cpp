#include "Enemy_Slash_Object.h"
#include "QuadTree.h"

Enemy_Slash_Object::Enemy_Slash_Object(VEC2 vec)
{
	m_TypeName = L"ENEMY_SLASH";

	m_node = Node(vec);

	m_body_size = VEC2(60, 60);

	m_rigid_body = Rect_Update();

	m_isStatic = false;
}

Enemy_Slash_Object::Enemy_Slash_Object(float x, float y)
{
	m_TypeName = L"ENEMY_SLASH";

	m_node = Node(VEC2(x,y));

	m_body_size = VEC2(60, 60);

	m_rigid_body = Rect_Update();

	m_isStatic = false;
}

Enemy_Slash_Object::~Enemy_Slash_Object()
{
}

bool Enemy_Slash_Object::Update()
{
	if (m_parent)
		m_parent->Destroy(this);
	return true;
}
