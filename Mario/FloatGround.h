#pragma once
#include "Object.h"
class FloatGround :
	public Object
{
public:
	FloatGround(float x, float y);
	bool Drop();
	bool Update() override;
	~FloatGround();

private: 
	VEC2 m_origin_pos;

	bool Init_Sprite() override;

	bool touched = false;
	int m_countdown;
	int m_before_drop;
};

