#include "QuadTree.h"


QuadTree::QuadTree(int X)
{
	Create_Child(X);
}


QuadTree::~QuadTree()
{
		//delete Left_bot;	
		//delete Left_top;
		//delete Right_top;
		//delete Right_bot; 
}

bool QuadTree::Create_Child(int X)
{
	if (X > 0)
	{
		if (Left_top == nullptr)
			Left_top = new QuadTree(X - 1);
		if (Left_bot == nullptr)
			Left_bot = new QuadTree(X - 1);
		if (Right_top == nullptr)
			Right_top = new QuadTree(X - 1);
		if (Right_bot == nullptr)
			Right_bot = new QuadTree(X - 1);
	}

	return false;
}

Object* QuadTree::Add(Object * object)
{
	RECT bound = object->get_Rect();

	if (Left_top != nullptr)
	{
		RECT node = Left_top->m_bound;
		if (bound.right <= node.right  && bound.bottom <= node.bottom)
			return Left_top->Add(object);	
	}
	
	if (Left_bot != nullptr)
	{
		RECT node = Left_bot->m_bound;
		if (bound.right <= node.right  && bound.top >= node.top)
			return Left_bot->Add(object);
	}

	if (Right_top != nullptr)
	{
		RECT node = Right_top->m_bound;
		if (bound.left >= node.left  && bound.bottom <= node.bottom)
			return Right_top->Add(object);
	}

	if (Right_bot != nullptr)
	{
		RECT node = Right_bot->m_bound;
		if (bound.left >= node.left  && bound.top >= node.top)
			return Right_bot->Add(object);
	}

	object->m_parent = this;
	m_object.push_back(object);
	return object;
	
}

bool QuadTree::Update()
{
	RECT bound = m_bound;
	RECT camera = m_camera->getRect();

	vector<Object*> result = vector<Object*>(0);

	sort(m_destroy_list.begin(), m_destroy_list.end());	//sort to remove duplicate
	m_destroy_list.erase(std::unique(m_destroy_list.begin(), m_destroy_list.end()), m_destroy_list.end());
	
	for (int i = m_destroy_list.size() - 1; i >= 0 ; i--)
	{
		//delete m_object[m_destroy_list[i]];
		m_object[m_destroy_list[i]] = nullptr;
		m_object.erase(m_object.begin() + m_destroy_list[i]);
	}
	//m_object.shrink_to_fit();
	m_destroy_list.clear();

	if (Math::isCollided(camera, m_bound))
		for (int i = 0; i < m_object.size(); i++)
		{
			m_object[i]->m_index = i;
			m_object[i]->Update();
		}

	if (Left_top != nullptr)
		Left_top->Update();
	if (Left_bot != nullptr)
		Left_bot->Update();
	if (Right_top != nullptr)
		Right_top->Update();
	if (Right_bot != nullptr)
		Right_bot->Update();

	return true;
}

bool QuadTree::Render()
{
	RECT camera = m_camera->getRect();

	m_object.shrink_to_fit();

	if (Math::isCollided(camera, m_bound))
		for (int i = 0; i < m_object.size(); i++)
		{
			m_object[i]->Render();
		}
		
	if (Left_top != nullptr)
		Left_top->Render();
	if (Left_bot != nullptr)
		Left_bot->Render();
	if (Right_top != nullptr)
		Right_top->Render();
	if (Right_bot != nullptr)
		Right_bot->Render();

	return true;
}

bool QuadTree::isNode()
{
	return m_isNode;
}

RECT QuadTree::Set_bound(RECT rect)
{
	m_bound = rect;

	if (Left_top != nullptr)
	{
		RECT child = RECT();
		child.top = rect.top;
		child.bottom = (rect.bottom + rect.top) / 2;
		child.left = rect.left;
		child.right = (rect.left + rect.right) / 2;
		Left_top->Set_bound(child);
	}

	if (Left_bot != nullptr)
	{
		RECT child = RECT();
		child.top = (rect.bottom + rect.top) / 2;
		child.bottom = rect.bottom;
		child.left = rect.left;
		child.right = (rect.left + rect.right) / 2;
		Left_bot->Set_bound(child);
	}

	if (Right_top != nullptr)
	{
		RECT child = RECT();
		child.top = rect.top;
		child.bottom = (rect.bottom + rect.top) / 2;
		child.left = (rect.left + rect.right) / 2;
		child.right = rect.right;
		Right_top->Set_bound(child);
	}
	if (Right_bot != nullptr)
	{
		RECT child = RECT();
		child.top = (rect.bottom + rect.top) / 2;
		child.bottom = rect.bottom;
		child.left = (rect.left + rect.right) / 2;
		child.right = rect.right;
		Right_bot->Set_bound(child);
	}

	return m_bound;
}

vector<Object*> QuadTree::getObject()
{
	RECT bound = m_bound;
	RECT camera = m_camera->getRect();
	vector<Object*> result = m_object;

	if (Left_top != nullptr)
	{
		vector<Object*> temp = Left_top->getObject();
		result.insert(result.end(), temp.begin(), temp.end());
	}
		
	if (Left_bot != nullptr)
	{
		vector<Object*> temp = Left_bot->getObject();
		result.insert(result.end(), temp.begin(), temp.end());
	}

	if (Right_top != nullptr)
	{
		vector<Object*> temp = Right_top->getObject();
		result.insert(result.end(), temp.begin(), temp.end());
	}

	if (Right_bot != nullptr)
	{
		vector<Object*> temp = Right_bot->getObject();
		result.insert(result.end(), temp.begin(), temp.end());
	}

	return result;
}

bool QuadTree::Destroy(int i)
{
	if (m_object.size() > i)
	{
		m_destroy_list.push_back(i);
	}
	else
		return false;
}

bool QuadTree::Destroy(Object* object)
{
	return Destroy(object->m_index);
}