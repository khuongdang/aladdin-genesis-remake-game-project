#include "HUD_Desert_Number.h"



HUD_Desert_Number::HUD_Desert_Number()
{
	Init_Sprite();
}


HUD_Desert_Number::~HUD_Desert_Number()
{

}

bool HUD_Desert_Number::Render(D3DXVECTOR2 vec, int index)
{
	if (index >= 0 && index <= 9)
	{
		if (index != m_index)
		{
			m_index = index;
			m_sprite = *m_State_Sprite[index];
		}

		if (m_sprite.isInitailized())
		{
			m_sprite.Render(vec, 1);
		}
	}

	return true;
}

bool HUD_Desert_Number::Render(float x, float y, int index)
{
	return Render(VEC2(x, y), index);
}

bool HUD_Desert_Number::Init_Sprite()
{
	m_State_Sprite[0] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\0.png", 18, 18, 1, 1);

	m_State_Sprite[1] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\1.png", 18, 18, 1, 1);

	m_State_Sprite[2] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\2.png", 18, 18, 1, 1);

	m_State_Sprite[3] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\3.png", 18, 18, 1, 1);

	m_State_Sprite[4] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\4.png", 18, 18, 1, 1);

	m_State_Sprite[5] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\5.png", 18, 18, 1, 1);

	m_State_Sprite[6] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\6.png", 18, 18, 1, 1);

	m_State_Sprite[7] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\7.png", 18, 18, 1, 1);

	m_State_Sprite[8] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\8.png", 18, 18, 1, 1);

	m_State_Sprite[9] = new Sprite(L"Resource\\Sprite\\HUD\\DesertNumber\\9.png", 18, 18, 1, 1);

	m_sprite = *m_State_Sprite[0];

	m_State_Sprite.shrink_to_fit();

	return true;
}
