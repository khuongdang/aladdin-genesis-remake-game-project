#pragma once
#include "Object.h"
#include "QuadTree.h"
class Player_Slash_Object :
	public Object
{
public:
	Player_Slash_Object(VEC2 vec);
	Player_Slash_Object(float x, float y);
	~Player_Slash_Object();

	bool Update() override;

private:
	int countdown;
};

