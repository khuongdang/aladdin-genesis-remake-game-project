#include "CutScene.h"
#include "Sound.h"
#include "Camera.h"

CutScene::CutScene() {};

CutScene::CutScene(LPDIRECT3DDEVICE9* d3ddv, LPDIRECT3DSURFACE9* back_buffer, LPDIRECT3DSURFACE9* surface)
{
	m_d3ddv = d3ddv;
	m_back_buffer = back_buffer;
	m_surface = surface;
	state = L"Dead";
	m_is_active = false;
	CutScene::m_Instance = *this;	
}


CutScene::~CutScene()
{
	if (m_back_buffer) { m_back_buffer = nullptr; delete m_back_buffer; }
	if (m_d3ddv) { m_d3ddv = nullptr; delete m_d3ddv; }
	if (m_surface) { m_surface = nullptr; delete m_surface; }
}

bool CutScene::is_Active()
{
	return m_is_active;
}

CutScene* CutScene::get_Instance()
{
	return &m_Instance;
}

bool CutScene::Init()
{
	D3DXCreateSprite(*m_d3ddv, &m_spriteHandler);
	return true;
}

VEC2 aladdin_pos = VEC2(0, 0);

bool CutScene::New(LPWSTR name)
{
	S_array.clear();
	state = name;
	Sound::GetInstance()->Stop();
	if (state == L"DEAD")
	{
		Sound::GetInstance()->LoadSound("Resource\\Sounds\\Boxing_Bell.wav", "Boxing_Bell");
		Sound::GetInstance()->Play("Boxing_Bell", true, 3);
		Sprite a = Sprite(L"Resource\\Sprite\\CutScene\\Dead.png", 178, 172, 31, 31,5);
		S_array.push_back(a);
	}
	else if (state == L"WIN")
	{
		Sound::GetInstance()->LoadSound("Resource\\Sounds\\LevelComplete.wav", "LevelComplete");
		Sound::GetInstance()->Play("LevelComplete");
		Sprite a = Sprite(L"Resource\\Sprite\\CutScene\\WIN\\font.png", 232, 141, 1, 1, 20);
		Sprite b = Sprite(L"Resource\\Sprite\\CutScene\\WIN\\aladdin.png", 52, 64, 10, 10, 5);
		Sprite c = Sprite(L"Resource\\Sprite\\CutScene\\WIN\\abu.png", 62, 39, 8, 8, 5);
		S_array.push_back(a);
		S_array.push_back(b);
		S_array.push_back(c);

		aladdin_pos = Camera::getInstance()->Get_Pos();
		aladdin_pos.x = Camera::getInstance()->getRect().right + 50;
		aladdin_pos.y += SCREEN_HEIGHT * 2 / 6;
	}

	m_is_active = true;
	
	return true;
}

bool CutScene::Run()
{	
	VEC2 pos = Camera::getInstance()->Get_Pos();

	

	if ((*m_d3ddv)->BeginScene())
	{
		// Clear back buffer with BLACK
		(*m_d3ddv)->ColorFill(*m_back_buffer, NULL, D3DCOLOR_XRGB(0, 0, 0));

		if (state == L"DEAD")
		{
			S_array[0].Render(pos, 1);
			S_array[0].Update();
		}
		else if (state == L"WIN")
		{
			S_array[0].Render(pos, 1);
			S_array[0].Update();
			S_array[1].Render(aladdin_pos.x-30, aladdin_pos.y, -1);
			S_array[1].Update();
			S_array[2].Render(aladdin_pos.x + 30, aladdin_pos.y, 1);
			S_array[2].Update();
			aladdin_pos.x -= 2;
		}


		(*m_d3ddv)->EndScene();

	}

	(*m_d3ddv)->Present(NULL, NULL, NULL, NULL);

	if (is_end())
	{
		m_is_active = false;
		Sound::GetInstance()->Stop();
	}
	return true;
}

bool CutScene::is_end()
{
	if (state == L"DEAD")
		return S_array[0].is_endFrame();
	if (state == L"WIN")
		return aladdin_pos.x < Camera::getInstance()->getRect().left - 60;
}



bool CutScene::End()
{
	return false;
}



CutScene CutScene::m_Instance;
