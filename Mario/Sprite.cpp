#include "Sprite.h"
#include "GameScene.h"



Sprite::Sprite() {};

Sprite::Sprite( LPWSTR Path, int Width, int Height, int Count, int SpritePerRow, int FrameCount,int StartFrame, int ActionFrame)
{
	m_inited = true;

	D3DXIMAGE_INFO info ;
	HRESULT result;

	m_image = NULL;
	m_spriteHandler = GameScene::getInstance()->getSpriteHandler();

	m_width = Width;
	m_height = Height;
	m_count = Count;
	m_spritePerRow = SpritePerRow;
	m_index = 0;
	m_isdone = false;
	m_frameCount = FrameCount;
	m_start_frame = StartFrame;
	m_action_frame = ActionFrame;

	m_scale = VEC3(1, 1, 1);
	m_center = VEC3(m_width / 2, m_height / 2, 0);
	m_facing = FACE_RIGHT;

	result = D3DXGetImageInfoFromFile(Path, &info);

	if (result != D3D_OK)
	{
		trace(L"[ERROR] Failed to get information from image file '%s'", Path);
		return;
	}

	(*m_spriteHandler)->GetDevice(&m_d3ddv);
	result = D3DXCreateTextureFromFileExW(
		m_d3ddv, Path,
		info.Width, info.Height,
		1,				//Mipmap levels
		D3DUSAGE_DYNAMIC,
		D3DFMT_UNKNOWN,
		D3DPOOL_DEFAULT,
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		D3DCOLOR_XRGB(0, 0, 0),		// Transparent color
		&info,				// Image information
		NULL,
		&m_image);
	
	if (result != D3D_OK)
	{
		trace(L"[ERROR] Failed to create texture from file '%s'", Path);
		return;
	}
}


Sprite::~Sprite()
{
	m_spriteHandler = nullptr; delete m_spriteHandler;
	m_d3ddv = nullptr; delete m_d3ddv;
	m_image = nullptr; delete m_image;
}

int Sprite::get_Index()
{
	return m_index;
}

int Sprite::get_Count()
{
	return m_count;
}

bool Sprite::Update()
{
	FrameCurrent++;
	int isdoneframe = FrameCurrent / m_frameCount;
	m_index += isdoneframe;

	if (m_index >= m_count-1 && !m_isdone)
		m_isdone = true;

	m_index = m_index % m_count + m_start_frame*(m_index / m_count);

	FrameCurrent %= m_frameCount;

	return true;
}

bool Sprite::Render(VEC2 vec, int state)
{
	return this->Render(vec.x, vec.y, state);
}

bool Sprite::Render(float X, float Y, int state)
{
	RECT srect;

	srect.left =  (m_index % m_spritePerRow)*(m_width);
	srect.top =   (m_index / m_spritePerRow)*(m_height);
	srect.right = srect.left + m_width - 1;
	srect.bottom = srect.top + m_height -1;

	(*m_spriteHandler)->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_OBJECTSPACE);

	m_position = VEC3( X-m_width/2 , Y-m_height/2 , 0);

	m_scale = VEC3(state, 1, 1);

	D3DXMatrixTransformation(&m_tranform, &m_center, NULL, &m_scale, &m_center, NULL, &m_position);

	(*m_spriteHandler)->SetTransform(&m_tranform);

	(*m_spriteHandler)->Draw(
		m_image,
		&srect,
		NULL,
		NULL,
		D3DCOLOR_XRGB(255, 255, 255)	// Hardcode!
	);
	(*m_spriteHandler)->End();

	return false;
}

int Sprite::get_height()
{
	return m_height;
}

int Sprite::get_width()
{
	return m_width;
}

bool Sprite::isdone()
{
	return m_isdone;
}

bool Sprite::is_endFrame()
{
	if (m_index == m_count-2)
		return true;
	return false;
}

bool Sprite::is_startFrame()
{
	if (m_index == 0)
		return true;
	return false;
}

bool Sprite::is_action_frame()
{
	if (m_index == m_action_frame)
		return true;
	return false;
}

bool Sprite::isInitailized()
{
	return m_inited;
}

