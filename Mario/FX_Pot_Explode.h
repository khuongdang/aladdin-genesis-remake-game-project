#pragma once
#include "Object.h"
#include "QuadTree.h"

class FX_Pot_Explode : public Object
{
public:
	FX_Pot_Explode(VEC2 pos);
	~FX_Pot_Explode();
	bool Update();
	bool Init_Sprite();
};

