#include "FrameWork.h"
#include "System.h"

bool pause = 0;

LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_ACTIVATE:
	{
		if (wParam == WA_INACTIVE)
			pause = 1;
		else
		{
			KeyboardManager::getInstance()->AcquireKeyboard();
			pause = 0;
		}
	}

		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);

}


FrameWork::FrameWork(HINSTANCE* hInstance, int nCmdShow)
{
	m_hInstance = *hInstance;
	m_nCmdShow = nCmdShow;
}


FrameWork::~FrameWork()
{
	delete m_hInstance;
	delete m_hWnd;
}

bool FrameWork::Init()
{
	m_wc.cbSize = sizeof(WNDCLASSEX);

	m_wc.style = CS_HREDRAW | CS_VREDRAW;
	m_wc.hInstance = m_hInstance;

	m_wc.lpfnWndProc = (WNDPROC)WinProc;
	m_wc.cbClsExtra = 0;
	m_wc.cbWndExtra = 0;
	m_wc.hIcon = NULL;
	m_wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	m_wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	m_wc.lpszMenuName = NULL;
	m_wc.lpszClassName = APP_CLASS;
	m_wc.hIconSm = NULL;

	if (!RegisterClassEx(&m_wc))
	{
		MessageBox(m_hWnd, L"Can't not register ClassEX", L"ERROR", MB_OK);
		return false;
	}
		

	m_hWnd = 
		CreateWindow(
			APP_CLASS,	//APP_CLASS,
			WINDOW_TITLE,	//MAIN_WINDOW_TITLE,
			WS_OVERLAPPEDWINDOW,
			//			WS_EX_TOPMOST | WS_VISIBLE | WS_POPUP,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			SCREEN_WIDTH,
			SCREEN_HEIGHT,
			NULL,
			NULL,
			m_hInstance,
			NULL);

	if (!m_hWnd) {
		MessageBox(m_hWnd, L"Can't not create Window Handler", L"ERROR", MB_OK);
		return false;
	}

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);

	KeyboardManager(&m_hInstance, &m_hWnd);

	if (KeyboardManager::getInstance() == NULL)
	{
		MessageBox(m_hWnd, L"Can't not create Keyboard Manager", L"ERROR", MB_OK);
		return false;
	}

	KeyboardManager::getInstance()->Init();

	DXManager(&m_hInstance, &m_hWnd);
	if (DXManager::getInstance() == NULL)
	{
		MessageBox(m_hWnd, L"Can't not create DirectX Manager", L"ERROR", MB_OK);
		return false;
	}

	DXManager::getInstance()->Init();

	return true;
}

bool FrameWork::Run()
{


	//// Enter game message loop
	MSG msg;
	int done = 0;
	DWORD frame_start = GetTickCount();
	DWORD tick_per_frame = 0;
	while (!done)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{		
			if (msg.message == WM_ACTIVATE)
			{
				while (msg.wParam == WA_INACTIVE) {};
			}
			if (msg.message == WM_QUIT) 
				done = 1;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		DWORD now = GetTickCount();
		DWORD time = now - frame_start;
		if (now - frame_start >= tick_per_frame && done == 0 && pause == 0)
		{
			frame_start = now;
			DXManager::getInstance()->Run();
		}
	}

	return true;
}

bool FrameWork::End()
{
	return false;
}
