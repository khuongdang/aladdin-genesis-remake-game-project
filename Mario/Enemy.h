#pragma once

#include "Knife.h"
#include "Pot.h"
#include "Spell.h"
#include "Enemy_Slash_Object.h"
#include "Moving_Flame.h"

#include "Hakim.h"
#include "Nahbi.h"
#include "Fazal.h"
#include "KnifeGuy.h"
#include "ThrowPot.h"
#include "HidePot.h"

#include "Jafar.h"