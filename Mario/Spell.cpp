#include "Spell.h"

Spell::Spell(float x, float y)
{
	m_TypeName = L"SPELL";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(30, 18);	//temp
	m_rigid_body = Rect_Update();
}

Spell::~Spell()
{
	m_parent = nullptr; delete m_parent;
	for (int i = 0; i < State_Sprite.size(); i++)
	{
		delete State_Sprite[i];
	}
	State_Sprite.clear();
}

bool Spell::Update()
{
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool Spell::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Projectile\\Spell.png", 30, 18, 3, 3, 2);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}

int Spell::Hit()
{
	return m_parent->Destroy(this);
}
