#include "Stair_With_Trigger.h"



Stair_With_Trigger::Stair_With_Trigger(float x, float y, float width, float height)
{
	m_TypeName = L"STAIR";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	m_body_size = VEC2(width, height);

	m_rigid_body = Rect_Update();

	m_isStatic = true;
}

Stair_With_Trigger::~Stair_With_Trigger()
{

}