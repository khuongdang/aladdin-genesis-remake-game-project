#pragma once

#include "System.h"
#include "Player.h"
#include "Apple.h"
#include "Camera.h"

#include "QuadTree.h"
#include "Object.h"
#include "Object_List.h"
#include "Enemy.h"



class GameScene
{
public:
	
	GameScene(LPDIRECT3DDEVICE9* d3ddv ,LPDIRECT3DSURFACE9* back_buffer, LPDIRECT3DSURFACE9* surface);
	~GameScene();

	bool Init();
	bool New();
	bool Run();
	bool End();

	bool Up();
	bool Down();
	bool Left();
	bool Right();
	bool Stand();

	bool X();
	bool Z();
	bool A();

	bool next_Scene(LPWSTR scene);

	bool Add_Score(int score);

	static GameScene* getInstance();

	LPD3DXSPRITE* getSpriteHandler();
	Apple * CreateApple(VEC2 pos, VEC2 velocity, VEC2 gravity = VEC2(0,0));
	Knife * CreateKnife(VEC2 pos, VEC2 velocity, VEC2 gravity = VEC2(0,0));
	Pot * CreatePot(VEC2 pos, VEC2 velocity, VEC2 gravity = VEC2(0, 0));
	Spell * CreateSpell(VEC2 pos, VEC2 velocity, VEC2 gravity = VEC2(0, 0));
	Object * Add_Object(Object * object);
	Object * Add_Player_layer(Object * object);
	Enemy_Slash_Object * Enemy_Slash(VEC2 pos);
	Moving_Flame * Create_Moving_Flame(VEC2 pos, VEC2 vel);
	Object * CreateFX(Object * FX);

	bool Respawn();

	bool Init_Snake_Boss_State();
	bool Toggle_God_Mode();
private:

	GameScene();

	LPWSTR state = L"Boss";	
	Player * Aladdin;
	Camera* m_camera;

	QuadTree m_Player_layer = QuadTree(0);
	QuadTree m_Objects = QuadTree(1);
	QuadTree m_Enemy = QuadTree(1);

	LPDIRECT3DDEVICE9* m_d3ddv = nullptr;
	LPDIRECT3DSURFACE9* m_back_buffer = nullptr;
	LPDIRECT3DSURFACE9* m_surface = nullptr;

	LPD3DXSPRITE m_spriteHandler;

	static GameScene m_Instance;

	bool Update_Object();
	bool Render();
	bool Colided(Object* a, Object* b);
	bool Colided_Up(Object * a, Object * b);
	bool Bounc(Object * a, Object * b, int type =  STATE_RUN_JUMP);
	bool Hanging(Object * a, Object * b);
	bool Climbing(Object * a, Object * b);
	bool LoadMap();
	bool LoadSound();

	bool Going_next_scene = false;

	VEC2 CheckPoint;

	bool God_Mode;
	bool Spawn_apple = false;
	bool Just_respawn = false;
};

