#pragma once
#include "Object.h"
class Item_Genie :
	public Object
{
public:
	Item_Genie(VEC2 pos);
	~Item_Genie();

	bool Init_Sprite();
	bool Update() override;
	int Hit();
};

