#pragma once
#include <d3d9.h>

static class Math
{
public:
	 Math();
	~Math();

	static float abs(float number);
	static long abs(long number);
	static bool isCollided(RECT a, RECT b);
};

