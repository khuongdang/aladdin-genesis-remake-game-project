#pragma once
#include "GroundObject.h"
class Camel :
	public GroundObject
{
public:
	Camel(int X = SCREEN_WIDTH / 2 + 200, int Y = SCREEN_HEIGHT - 80);
	~Camel();

	bool Update() override;
	int Hit() override;

private:
	bool Init_Sprite();
};

