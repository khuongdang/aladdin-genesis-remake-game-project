#pragma once
#include "Object.h"
#include "QuadTree.h"
class FX_Fire_Coal :
	public Object
{
public:
	FX_Fire_Coal(VEC2 pos);
	~FX_Fire_Coal();
	bool Update();
	bool Init_Sprite();
};

