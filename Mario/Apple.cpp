#include "Apple.h"
#include "GameScene.h"

#include "FXmanager.h"

Apple::Apple(int x, int y)
{
	m_TypeName = L"APPLE";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(7, 7);	//temp
	m_rigid_body = Rect_Update();
}

bool Apple::Update()
{
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool Apple::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Apple\\Idle.png", 7, 7, 1, 1);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}

int Apple::Hit()
{
	FXmanager::get_instance()->CreateFX(L"Apple_Explode", m_node.get_Position());
	return m_parent->Destroy(this);
}

Apple::~Apple()
{
}
