#pragma once

#define EXPLODE_ON_DEAD L"Explode_On_Dead"
#include "FX_Explode_On_Dead.h"

#define POT_EXPLODE L"Pot_Explode"
#include "FX_Pot_Explode.h"

#include "FX_Fire_Coal.h"

#include "FX_Apple_Explode.h"