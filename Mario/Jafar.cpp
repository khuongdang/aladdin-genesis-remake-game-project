#include "Jafar.h"
#include "GameScene.h"
#include "FXmanager.h"
#include "CutScene.h"

Jafar::Jafar(int X, int Y, Player* Following)
{
	m_TypeName = L"BOSS";
	m_Following = Following;

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_node.Set_maxSpeed(0);
	m_node.Set_friction(0);
	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_facing = FACE_LEFT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(40, 60);
	m_anchor_point = VEC2(0, 16);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_heath = 8;
	m_isGrounded = false;
	m_isStatic = false;

	m__state = 0;
	
}


Jafar::~Jafar()
{
}

bool Jafar::Update()
{
	if (m_Following != nullptr)
	{
		VEC2 player = m_Following->get_Position();
		VEC2 pos = m_node.get_Position();

		float distance = player.x - pos.x;

		if (distance > 0)
			Move_Right();
		else
			Move_Left();
		Throw();
	}

	if (m_countdown > 0)
		m_countdown--;

	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool Jafar::Throw()
{
	if (m_immute > 0)
		m_immute--;

	if(m_state == STATE_THROW)
		if (m_sprite.is_action_frame())
		{
			if (m__state == 0)
			{
				if (m_countdown == 0)
				{

					m_countdown = 3;
					VEC2 player = m_Following->get_Position();
					float distancex = player.x - m_node.x();
					float distancey = player.y - m_node.y() + 10;
					if (distancex != 0)
					{
						float vecy = (10 * m_facing * distancey) / distancex;
						VEC2 velocity = VEC2(10 * m_facing, vecy);
						GameScene::getInstance()->CreateSpell(m_node.get_Position(), velocity);
						return true;
					}
				}
			}
			else if (m__state == 1)
			{
				if (!attacked)
				{
					attacked = true;
					VEC2 vel = VEC2(3 * m_facing, 0);
					VEC2 pos = m_node.get_Position();
					pos.y += 10;
					GameScene::getInstance()->Create_Moving_Flame(pos, vel);
					return true;
				}
			}
			
		}
		else {
			attacked = false;
		}

	Change_State(STATE_THROW);
}

bool Jafar::Move_Left()
{
	m_facing = FACE_LEFT;
	return true;
}

bool Jafar::Move_Right()
{
	m_facing = FACE_RIGHT;
	return true;
}

bool Jafar::Init_Sprite()
{
	State_Sprite.resize(10);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Boss\\Human\\Idle.png", 87, 73, 4, 4, 5);

	State_Sprite[STATE_STUNT] = new Sprite(L"Resource\\Sprite\\Enemy\\Boss\\Human\\Idle.png", 87, 73, 4, 4, 5);

	State_Sprite[STATE_THROW] = new Sprite(L"Resource\\Sprite\\Enemy\\Boss\\Human\\Throw.png", 87, 73, 4, 4, 5, 3, 3);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();
	return true;
}

bool Jafar::Init_Snake()
{
	m__state = 1;

	m_heath = 8;

	GameScene::getInstance()->Init_Snake_Boss_State();

	State_Sprite.resize(10);

	State_Sprite[STATE_THROW] = new Sprite(L"Resource\\Sprite\\Enemy\\Boss\\Snake\\Throw.png", 111, 86, 11, 11, 6, 0, 10);

	m_sprite = *State_Sprite[STATE_THROW];

	State_Sprite.shrink_to_fit();
	return true;
}

int Jafar::Hit()
{
	if (m_immute == 0)
	{
		m_heath--;
		if (m__state == 0)
		{

			if (State_Sprite.size() > 1 && State_Sprite[STATE_STUNT] != nullptr)
			{
				m_countdown = 50;
				m_Blocking_Sprite = false;
				Change_State(STATE_STUNT, true);
				m_node.set_Accelerate(0, 0);
				m_node.set_Velocity(0, 0);
				m_immute = 90;
			}
			if (m_heath <= 0)
			{
				Init_Snake();
			}
		}
		if (m__state == 1)
		{
			if (m_heath <= 0)
			{
				m_parent->Destroy(this);
				FXmanager::get_instance()->CreateFX(L"Explode_On_Dead", m_node.get_Position());
				CutScene::get_Instance()->New(L"WIN");
			}
		}
	}
	return m_heath;
}
