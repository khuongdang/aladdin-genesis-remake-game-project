#pragma once
#include "System.h"
#include "Sprite.h"

using namespace std;

class HUD_Heath
{
public:
	HUD_Heath();
	~HUD_Heath();

	bool Render(VEC2 vec, int health);

	bool Render(float x, float y, int health);
private:
	Sprite m_sprite = Sprite();
	vector<Sprite*> m_State_Sprite = vector<Sprite*>(9);
	bool Init_Sprite();
	VEC2 m_Position;
	int m_heath;
};

