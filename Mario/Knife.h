#pragma once
#include "Projectile.h"
#include "QuadTree.h"

class Knife :
	public Projectile
{
public:
	Knife(float x, float y);
	bool Update() override;
	int Hit();
	~Knife();
private:
	bool Init_Sprite();
};

