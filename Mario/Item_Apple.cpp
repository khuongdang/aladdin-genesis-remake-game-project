#include "Item_Apple.h"
#include "QuadTree.h"
#include "Sound.h"
#include "GameScene.h"

Item_Apple::Item_Apple(float X, float Y)
{
	m_TypeName = L"ITEM_APPLE";

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(13, 14);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Item_Apple::~Item_Apple()
{
}

bool Item_Apple::Init_Sprite()
{
	State_Sprite.resize(2);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Item\\Apple\\apple.png", 13, 14, 1, 1);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}

int Item_Apple::Hit()
{
	Sound::GetInstance()->Play("Apple_Collect");
	GameScene::getInstance()->Add_Score(50);
	if (m_parent != nullptr) m_parent->Destroy(this);
	return 0;
}


