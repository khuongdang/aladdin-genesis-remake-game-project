#include "SpringBoard.h"
#include "Sound.h"


SpringBoard::SpringBoard(int X, int Y)
{
	m_TypeName = L"SPRINGBOARD";

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(50, 22);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


SpringBoard::~SpringBoard()
{
}

bool SpringBoard::Update()
{
	if (m_state == STATE_STUNT)
	{
		if (m_sprite.isdone())
			Change_State(STATE_IDLE);
	}
	return true;
}

int SpringBoard::Hit()
{
	Change_State(STATE_IDLE);
	Change_State(STATE_STUNT);
	Sound::GetInstance()->Play("Canopy Bounce");
	return 0;
}

bool SpringBoard::Init_Sprite()
{
	State_Sprite.resize(2);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\SpringBoard\\Idle.png", 50, 37,1,	1);

	State_Sprite[STATE_STUNT] = new Sprite(L"Resource\\Sprite\\SpringBoard\\Stunted.png", 50, 37,10,10,4);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}
