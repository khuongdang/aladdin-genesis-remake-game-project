#include "ThrowPot.h"
#include "GameScene.h"


ThrowPot::ThrowPot(int X, int Y, Player* Following)
{
	m_TypeName = L"AIR";
	m_Following = Following;

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_body_size = VEC2(0, 0);
	//m_anchor_point = VEC2(0, 0);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_isStatic = false;
}


ThrowPot::~ThrowPot()
{
}

bool ThrowPot::Update()
{
	if (m_Following != nullptr)
	{
		VEC2 player = m_Following->get_Position();
		VEC2 pos = m_node.get_Position();

		float distance = player.x - pos.x;
		float distanceY = player.y - pos.y;

		if (Math::abs(distance) <= 60 && distanceY>0 && distanceY<150)
		{
			Throw();
		}
		else 
		{
			Idle();
		}
	}

	return true;
}

bool ThrowPot::Throw()
{
	
	Change_State(STATE_SLASH, true);
	if (m_state == STATE_SLASH)
	{
		if (m_sprite.is_action_frame())
		{
			if (!attacked)
			{
				VEC2 pos = m_node.get_Position();
				pos.x += 23; //added so that the pot animation is correct placed
				GameScene::getInstance()->CreatePot(pos, VEC2(1, -2), GRAVITY);
				attacked = true;
			};
		}
		else 
			attacked = false;
	}
	return true;
}

bool ThrowPot::Idle()
{
	Change_State(STATE_IDLE);
	return true;
}

bool ThrowPot::Init_Sprite()
{
	State_Sprite.resize(10);

	State_Sprite[STATE_IDLE] = new Sprite();

	State_Sprite[STATE_SLASH] = new Sprite(L"Resource\\Sprite\\Enemy\\ThrowPot\\Slash.png", 80, 50, 10, 5, 5, 0, 6);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();
	return true;
}
