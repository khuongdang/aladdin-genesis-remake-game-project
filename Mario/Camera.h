#pragma once
#include "System.h"
#include "Object.h"
#include "HUD.h"


class Camera
{
public:
	Camera(LPDIRECT3DDEVICE9 *d3ddv, Object* object = nullptr, RECT bound = RECT() );
	~Camera();

	Object* follow(Object* object);
	Object* following();
	Object* unfollowing();

	bool Update();
	bool Render();
	RECT getRect();
	bool Lookup();
	VEC2 Get_Pos();
	static Camera* getInstance();

private:
	static Camera m_instance;
	RECT m_bound;
	Object* m_following = nullptr;
	LPDIRECT3DDEVICE9* m_device = nullptr;

	D3DXMATRIX m_orthographicMatrix;
	D3DXMATRIX m_identityMatrix;
	D3DXMATRIX m_viewMatrix;

	HUD_Heath* m_HUD_health = nullptr;
	HUD_Apple* m_HUD_Apple = nullptr;
	HUD_Ruby* m_HUD_Rupy = nullptr;
	HUD_Score* m_HUD_Score = nullptr;
	HUD_Lives* m_HUD_Lives = nullptr;
	bool Render_Hud();

	VEC2 m_position;
};

