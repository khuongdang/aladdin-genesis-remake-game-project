#include "KeyboardManager.h"

KeyboardManager::KeyboardManager() {};

KeyboardManager::KeyboardManager(HINSTANCE* hInstance, HWND* hWnd)
{
	m_hwnd = hWnd;
	m_hInstance = hInstance;
	m_Instance = *this;
}

bool KeyHold[1028];

KeyboardManager::~KeyboardManager()
{
}

bool KeyboardManager::Init()
{

		HRESULT result;
		result = DirectInput8Create(*m_hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_di, NULL);
		if (result != DI_OK)
			return 0;

		result = m_di->CreateDevice(GUID_SysKeyboard, &m_didev, NULL);

		if (result != DI_OK)
		{
			trace(L"Create Device for keyboard not successfully");
			return false;
		}

		result = m_didev->SetDataFormat(&c_dfDIKeyboard);

		if (result != DI_OK)
		{
			trace(L"SetDataFormat for keyboard not successfully");
			return false;
		}
		

		result = m_didev->SetCooperativeLevel(*m_hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

		if (result != DI_OK)
		{
			trace(L"SetCooperativeLevel for keyboard not successfully");
			return false;
		}


		// IMPORTANT STEP TO USE BUFFERED DEVICE DATA!
		//
		// DirectInput uses unbuffered I/O (buffer size = 0) by default.
		// If you want to read buffered data, you need to set a nonzero
		// buffer size.
		//
		// Set the buffer size to DINPUT_BUFFERSIZE (defined above) elements.
		//
		// The buffer size is a DWORD property associated with the device.
		DIPROPDWORD dipdw;

		dipdw.diph.dwSize = sizeof(DIPROPDWORD);
		dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		dipdw.diph.dwObj = 0;
		dipdw.diph.dwHow = DIPH_DEVICE;
		dipdw.dwData = KEYBOARD_BUFFER_SIZE; // Arbitary buffer size

		

		result = m_didev->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph);
		if (result != DI_OK)
		{
			trace(L"SetProperty for keyboard not successfully");
			return false;
		}

		result = m_didev->Acquire();

		if (result != DI_OK)
		{
			trace(L"Acquire keyboard not successfully");
			return false;
		}

	return true;
}

bool KeyboardManager::ProcessKeyboard()
{
	if (m_didev != nullptr)
	{
		m_didev->GetDeviceState(sizeof(m_KeyStates), m_KeyStates);
		if (IsKeyDown(DIK_ESCAPE))
		{
			trace(L"Escape key pressed!");
			PostMessage(*m_hwnd, WM_QUIT, 0, 0);
		}

		// Collect all buffered events
		DWORD dwElements = KEYBOARD_BUFFER_SIZE;
		HRESULT result = m_didev->GetDeviceData(sizeof(DIDEVICEOBJECTDATA), m_KeyEvents, &dwElements, 0);

		// Scan through all data, check if the key is pressed or released
		for (DWORD i = 0; i < dwElements; i++)
		{
			int KeyCode = m_KeyEvents[i].dwOfs;
			if (IsKeyDown(KeyCode))
				OnKeyDown(KeyCode);
			else
				OnKeyUp(KeyCode);
		}

		if (!OnkeyHold())
			GameScene::getInstance()->Stand();

		return true;
	}
	return false;
}
bool KeyboardManager::OnkeyHold()
{
	bool result = false;

	if (KeyHold[DIK_LEFT])
	{
		GameScene::getInstance()->Left();
		result = true;
	}	
	else if (KeyHold[DIK_RIGHT])
	{
		GameScene::getInstance()->Right();
		result = true;
	} 

	if (KeyHold[DIK_UP])
	{
		GameScene::getInstance()->Up();
		result = true;
}
	else if (KeyHold[DIK_DOWN])
	{
		GameScene::getInstance()->Down();
		result = true;
	}

	return result;
}

bool KeyboardManager::IsKeyDown(int KeyCode)
{
		return (m_KeyStates[KeyCode] & 0x80) > 0;
}

bool KeyboardManager::OnKeyUp(int KeyCode)
{
	KeyHold[KeyCode] = false;

	if (KeyCode == DIK_DOWN)
	{
		GameScene::getInstance()->Stand();
	}

	if (KeyCode == DIK_UP)
	{
		GameScene::getInstance()->Stand();
	}
		

	if (!IsKeyDown(DIK_LEFT))
	{
		GameScene::getInstance()->Stand();
	}
		

	if (!IsKeyDown(DIK_RIGHT))
	{
		GameScene::getInstance()->Stand();
	}

	return true;
}

bool KeyboardManager::OnKeyDown(int KeyCode)
{
	if (KeyCode == DIK_X)
	{
		GameScene::getInstance()->X();
	}

	if (KeyCode == DIK_Z)
	{
		GameScene::getInstance()->Z();
	}

	if (KeyCode == DIK_A)
	{
		GameScene::getInstance()->A();
	}

	if (KeyCode == DIK_H)
	{
		if (KeyHold[DIK_LCONTROL])
			if (KeyHold[DIK_K])
				GameScene::getInstance()->Toggle_God_Mode();
	}

	KeyHold[KeyCode] = true;

	return true;
}

KeyboardManager* KeyboardManager::getInstance()
{
	return &KeyboardManager::m_Instance;
}

bool KeyboardManager::AcquireKeyboard()
{
	if (m_didev != NULL)
		m_didev->Acquire();
	return true;
}

KeyboardManager KeyboardManager::m_Instance;
