#pragma once
#include "GroundEnemy.h"
class HidePot :
	public GroundEnemy
{
public:
	HidePot(int X, int Y, Player* Following = nullptr);
	~HidePot();

	bool Update() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Slash();

	bool Stand();

private:


	bool Init_Sprite() override;
	

};

