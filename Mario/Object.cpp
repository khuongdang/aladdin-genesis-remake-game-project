#include "Object.h"



Object::Object()
{
}

bool Object::Render()
{
	if (m_sprite.isInitailized())
	{
		if(m_immute % 5 != 1)
			m_sprite.Render(m_node.get_Position(), m_facing);
		m_sprite.Update();
	}
	return true;
}

VEC2 Object::set_Position(float x, float y)
{
	return m_node.set_Position(VEC2(x, y));
}

VEC2 Object::get_Position()
{
	return m_node.get_Position();
}

VEC2 Object::set_Gravity(float x, float y)
{
	return m_node.set_Gravity(x, y);
}

VEC2 Object::set_Gravity(VEC2 vec)
{
	return m_node.set_Gravity(vec);
}

bool Object::Traslate(float x, float y)
{
	m_node.Translate(x, y);
	return true;
}

bool Object::Translate(VEC2 vec)
{
	m_node.Translate(vec);
	m_rigid_body = Rect_Update();
	return true;
}


VEC2 Object::set_Accelerator(VEC2 vec)
{
	return m_node.set_Accelerate(vec);
}

VEC2 Object::set_Accelerator(float x, float y)
{
	return m_node.set_Accelerate(x,y);
}

VEC2 Object::get_Accelerator()
{
	return m_node.get_Accelerate();
}

bool Object::set_Velocity(VEC2 vec)
{
	return m_node.set_Velocity(vec);
}

bool Object::set_Velocity(float x, float y )
{
	return m_node.set_Velocity(x, y);
}

VEC2 Object::get_Velocity()
{
	return m_node.get_Velocity();
}


bool Object::set_bodySize(VEC2 vec)
{
	m_body_size = vec;
	return true;
}

bool Object::set_bodySize(int x, int y)
{
	this->set_bodySize(VEC2(x, y));
	return true;
}

bool Object::set_anchorPoint(VEC2 vec)
{
	m_anchor_point = vec;
	return true;
}

bool Object::set_anchorPoint(int x, int y)
{
	m_anchor_point = VEC2(x, y);
	return false;
}

bool Object::set_Static(bool param)
{
	m_isStatic = param;
	return true;
}

bool Object::get_isStatic()
{
	return m_isStatic;
}

int Object::get_facing()
{
	return m_facing;
}

RECT Object::Rect_Update()
{
	VEC2 point = m_node.get_Position() + m_anchor_point;

	m_rigid_body.top =		point.y - m_body_size.y / 2;
	m_rigid_body.bottom =	point.y + m_body_size.y / 2;
	m_rigid_body.left =		point.x - m_body_size.x / 2;
	m_rigid_body.right =	point.x + m_body_size.x / 2;

	return m_rigid_body;
}

bool Object::Change_State(int State, bool lock)
{

	if (m_Blocking_Sprite)
	{
		m_Blocking_Sprite = !m_sprite.isdone();
	}

	if (!m_Blocking_Sprite)
		if (State != m_state)
		{
			m_sprite = *State_Sprite[State];
			m_state = State;
			m_Blocking_Sprite = lock;
			return true;
		}
	return false;
}