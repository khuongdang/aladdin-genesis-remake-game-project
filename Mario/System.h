#pragma once

#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>
#include <vector>
#include <string>


#include "Deffinition.h"
#include "Math.h"

#include "Node.h"

#include "trace.h"