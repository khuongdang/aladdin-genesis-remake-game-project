#include "FX_Apple_Explode.h"
#include "QuadTree.h"
#include "Sound.h"


FX_Apple_Explode::FX_Apple_Explode(VEC2 pos)
{
	m_TypeName = L"FX";

	m_node = Node(pos);

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_rigid_body = Rect_Update();
	Init_Sprite();

	Sound::GetInstance()->Play("Apple_Splat");
}


FX_Apple_Explode::~FX_Apple_Explode()
{
}

bool FX_Apple_Explode::Update()
{
	if (m_sprite.is_endFrame())
	{
		m_parent->Destroy(this);
		return false;
	}
	return true;
}

bool FX_Apple_Explode::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\FX\\Apple_Explode.png", 31, 27, 5, 5, 4);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
