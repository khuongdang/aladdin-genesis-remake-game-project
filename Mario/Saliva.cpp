#include "Saliva.h"
#include "QuadTree.h"


Saliva::Saliva(VEC2 pos)
{
	m_TypeName = L"APPLE";

	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(14, 12);	//temp
	m_rigid_body = Rect_Update();
}


Saliva::~Saliva()
{
}

bool Saliva::Update()
{
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

int Saliva::Hit()
{
	return m_parent->Destroy(this);
}

bool Saliva::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Saliva\\Idle.png", 14, 12, 8, 8, 5);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
