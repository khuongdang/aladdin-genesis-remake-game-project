#include "Camera.h"
#include "Player.h"

Camera::Camera(LPDIRECT3DDEVICE9 *d3ddv, Object* object, RECT bound)
{
	m_following = object;
	m_device = d3ddv;
	m_bound.top = bound.top + SCREEN_HEIGHT /2;
	m_bound.bottom = bound.bottom - SCREEN_HEIGHT / 2;
	m_bound.left = bound.left + SCREEN_WIDTH / 2;
	m_bound.right = bound.right - SCREEN_WIDTH / 2;
	D3DXMatrixOrthoLH(&m_orthographicMatrix, SCREEN_WIDTH, -SCREEN_HEIGHT, 0.0f, 1.0f);
	D3DXMatrixIdentity(&m_identityMatrix);
	m_instance = *this;

	m_HUD_health = nullptr;
	m_HUD_Apple = nullptr;
	m_HUD_Score = nullptr;
}

bool m_lookup = false;
int up_distance = 0;

Camera::~Camera()
{
	if (m_following) { m_following = nullptr; delete m_following; }
	if (m_device) { m_device = nullptr; delete m_device; }
}

Object * Camera::follow(Object * object)
{
	m_following = object;
	return m_following;
}

Object* Camera::following()
{
	return m_following;
}

Object* Camera::unfollowing()
{
	m_following = nullptr;
	return m_following;
}

bool Camera::Update()
{

	m_position = VEC2(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	if (m_following != nullptr)
	{
		m_position = m_following->get_Position();
	}

	if (m_lookup)
	{
		if (up_distance < SCREEN_HEIGHT/8)
			up_distance += 5;
		else
			up_distance = SCREEN_HEIGHT / 8;
		m_lookup = false;
	}
	else
	{
		if (up_distance > 0)
			up_distance -= 5;
		else
			up_distance = 0;
	}

	if (m_position.y < m_bound.top)
		m_position.y = m_bound.top;
	if (m_position.y > m_bound.bottom)
		m_position.y = m_bound.bottom;
	if (m_position.x < m_bound.left)
		m_position.x = m_bound.left;
	if (m_position.x > m_bound.right)
		m_position.x = m_bound.right;

	m_position.y -= up_distance;
	return true;
}



bool Camera::Render()
{
	Render_Hud();

	m_viewMatrix = D3DXMATRIX(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		-m_position.x, -m_position.y, 0, 1
	);

	if (m_device)
	{
		(*m_device)->SetTransform(D3DTS_PROJECTION, &m_orthographicMatrix);
		(*m_device)->SetTransform(D3DTS_WORLD, &m_identityMatrix);
		(*m_device)->SetTransform(D3DTS_VIEW, &m_viewMatrix);
		return true;
	}

	return false;
}

bool Camera::Render_Hud()
{
	// Render Health
	if (m_HUD_health == nullptr)
		m_HUD_health = new HUD_Heath();
	VEC2 pos_health = m_position;
	pos_health.x -= SCREEN_WIDTH * 1.8 / 6;
	pos_health.y -= SCREEN_HEIGHT * 2 / 6;
	m_HUD_health->Render(pos_health, ((Player*)m_following)->get_Health());

	// Render Apple
	if (m_HUD_Apple == nullptr)
		m_HUD_Apple = new HUD_Apple();
	VEC2 pos_apple = m_position;
	pos_apple.x += SCREEN_WIDTH * 2 / 6;
	pos_apple.y += SCREEN_HEIGHT * 2.5 / 6;
	m_HUD_Apple->Render(pos_apple, ((Player*)m_following)->Get_apple_amount());
	 
	// Render Ruby

	if (m_HUD_Rupy == nullptr)
		m_HUD_Rupy = new HUD_Ruby();
	VEC2 pos_ruby = m_position;
	pos_ruby.x += SCREEN_WIDTH * 1.2 / 6;
	pos_ruby.y += SCREEN_HEIGHT * 2.5 / 6;
	m_HUD_Rupy->Render(pos_ruby, ((Player*)m_following)->Get_ruby_amount());

	//Render Score
	if (m_HUD_Score == nullptr)
		m_HUD_Score = new HUD_Score();
	VEC2 pos_score = m_position;
	pos_score.x += SCREEN_WIDTH * 1.5 / 6;
	pos_score.y -= SCREEN_HEIGHT * 2.0 / 6;
	m_HUD_Score->Render(pos_score, ((Player*)m_following)->Get_score());

	// Render Life

	if (m_HUD_Lives == nullptr)
		m_HUD_Lives = new HUD_Lives();
	VEC2 pos_live = m_position;
	pos_live.x -= SCREEN_WIDTH * 2 / 6;
	pos_live.y += SCREEN_HEIGHT * 2.5 / 6;
	m_HUD_Lives->Render(pos_live, ((Player*)m_following)->Get_live());

	return true;
}

RECT Camera::getRect()
{
	RECT result = RECT();

	result.left = m_position.x - SCREEN_WIDTH / 2;
	result.right = m_position.x + SCREEN_WIDTH / 2;
	result.top = m_position.y - SCREEN_HEIGHT / 2;
	result.bottom = m_position.y + SCREEN_HEIGHT / 2;

	return result;
}

bool Camera::Lookup()
{
	m_lookup = true;
	return true;
}

VEC2 Camera::Get_Pos()
{
	return m_position;
}

Camera* Camera::getInstance()
{
	return &m_instance;
}

Camera Camera::m_instance = nullptr;
