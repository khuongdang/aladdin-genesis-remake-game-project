#pragma once
#include "GroundObject.h"
#include "QuadTree.h"
#include "Player.h"

class GroundEnemy :
	public GroundObject
{
public:
	GroundEnemy();
	~GroundEnemy();

	int Hit();
	bool is_Death();

	Player* m_Following;
protected:
	
	bool attacked = false;
	int m_heath = 3;
	
};

