#pragma once
#include "GroundEnemy.h"
#include "Player.h"
class Hakim :
	public GroundEnemy
{
public:
	Hakim(int X, int Y, Player* Following = nullptr);
	~Hakim();

	bool Update() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Slash();

	bool Stand();

private:

	Player* m_Following;
	bool Init_Sprite() override;
};

