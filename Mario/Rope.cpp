#include "Rope.h"



Rope::Rope(float x, float y, float width, float height)
{
	m_TypeName = L"ROPE";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	m_body_size = VEC2(width, height);

	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Rope::~Rope()
{
}
