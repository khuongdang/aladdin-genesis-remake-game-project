#include "HUD_Behind_Back_Ground.h"


HUD_Behind_Back_Ground::HUD_Behind_Back_Ground()
{

}


HUD_Behind_Back_Ground::~HUD_Behind_Back_Ground()
{
}

bool HUD_Behind_Back_Ground::Render()
{
	
	for (int i = m_State_Sprite.size() - 1; i>=0 ; i--)
	{
		VEC2 pos = m_camera->Get_Pos();
		if (i % 2 == 0)
		{
			int x = ((int)pos.x/20) % 60;
			if (x <= 29)
			{
				pos.x += x;
			}
			else {
				pos.x += 59 - x;
			}
		}
		else
		{
			int x = ((int)pos.x / 20) % 60;
			if (x <= 29)
			{
				pos.x -= x;
			}
			else {
				pos.x -= 59 - x;
			}
		}

		int y = ((int)pos.y / 5) % 20;
		if (y <= 9)
		{
			pos.y -= y;
		}
		else {
			pos.y -= 20 - y;
		}


		m_State_Sprite[i]->Render(pos, 1);
		pos.x += m_State_Sprite[i]->get_width() - 1;
		m_State_Sprite[i]->Render(pos,1);
	}
	return true;
}

Sprite * HUD_Behind_Back_Ground::add_sprite(Sprite * sprite)
{
	m_State_Sprite.push_back(sprite);
	return sprite;
}

Camera * HUD_Behind_Back_Ground::set_cam(Camera * cam)
{
	m_camera = cam;
	return m_camera;
}

bool HUD_Behind_Back_Ground::Init_Sprite()
{
	return false;
}
