#include "Fazal.h"
#include "GameScene.h"

Fazal::Fazal(int X, int Y, Player* Following)
{
	m_TypeName = L"ENEMY";
	m_Following = Following;

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos, GRAVITY);

	Init_Sprite();

	m_node.Set_maxSpeed(PLAYER_MAX_SPEED - 3);
	m_node.Set_friction(PLAYER_FRICTION);
	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_facing = FACE_LEFT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(50, 60);
	//m_anchor_point = VEC2(0, 0);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_isGrounded = false;
	m_isStatic = false;
	m_heath = 1;
}

Fazal::~Fazal()
{
}

bool Fazal::Update()
{
	if (m_Following != nullptr)
	{
		VEC2 player = m_Following->get_Position();
		VEC2 pos = m_node.get_Position();

		float x = player.x - pos.x;
		float y = player.y - pos.y;

		if (Math::abs(x) <= 150 && abs(y)<50)
		{
			if (x > 0)
				Move_Right();
			else
				Move_Left();
			Throw();
		}
		else if (Math::abs(x) <= 250 && abs(y)<50)
		{
			if (x > 0)
				Move_Right();
			else
				Move_Left();
		}
		else
		{
			Stand();
		}
	}


	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool Fazal::Move_Left()
{
	switch (m_state)
	{
	case STATE_RUN:
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_THROW:
		m_facing = FACE_LEFT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_IDLE:
		Change_State(STATE_RUN);
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;
	}
	return false;
}

bool Fazal::Move_Right()
{
	switch (m_state)
	{
	case STATE_RUN:
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_THROW:
		m_facing = FACE_RIGHT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_IDLE:
		Change_State(STATE_RUN);
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;
	}
	return false;
}

bool Fazal::Throw()
{

	if (m_state == STATE_THROW)
	{
		if (m_sprite.is_action_frame())
		{
			if (!attacked)
			{
				GameScene::getInstance()->CreateKnife(m_node.get_Position(), VEC2( 10*m_facing , -3), VEC2(0,0.5) );
				attacked = true;
			};
		}
		else
			if(m_sprite.is_startFrame())
			attacked = false;
	}

	switch (m_state)
	{
	case STATE_IDLE:
		Change_State(STATE_THROW, true);
		return true;
	case STATE_RUN:
		m_node.set_Velocity(0, 0);
		m_node.set_Accelerate(0, 0);
		Change_State(STATE_THROW, true);
		return true;
	default:
		return false;
	}

	return false;
}

bool Fazal::Stand()
{
	if (is_Grounded())
		switch (m_state)
		{
		case STATE_RUN:
			m_node.set_Accelerate(0, 0);
			Change_State(STATE_IDLE);
			return true;
		case STATE_THROW:
			Change_State(STATE_IDLE);
			return true;
		default:
			return false;
		}
}

bool Fazal::Init_Sprite()
{
	State_Sprite.resize(10);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Fazal\\Idle.png", 80, 80, 7, 7, 7);

	State_Sprite[STATE_RUN] = new Sprite(L"Resource\\Sprite\\Enemy\\Fazal\\Run.png", 60, 80, 8, 8, 5);

	State_Sprite[STATE_THROW] = new Sprite(L"Resource\\Sprite\\Enemy\\Fazal\\Throw.png", 80, 80, 5, 5, 8, 0, 3);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();
	return true;
}
