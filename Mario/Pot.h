#pragma once
#include "Projectile.h"
#include "QuadTree.h"
class Pot :
	public Projectile
{
public:
	Pot(float x, float y);
	bool Update() override;
	int Hit();
	~Pot();

private:
	bool Init_Sprite();
};

