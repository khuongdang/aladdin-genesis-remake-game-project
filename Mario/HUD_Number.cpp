#include "HUD_Number.h"



HUD_Number::HUD_Number()
{
	Init_Sprite();
}


HUD_Number::~HUD_Number()
{
}


bool HUD_Number::Render(VEC2 vec, int index)
{
	if (index >= 0 && index <= 9)
	{
		if (index != m_index)
		{
			m_index = index;
			m_sprite = *m_State_Sprite[index];
		}

		if (m_sprite.isInitailized())
		{
			m_sprite.Render(vec, 1);
		}
	}

	return true;
}

bool HUD_Number::Render(float x, float y, int index)
{
	return Render(VEC2(x,y),index);
}

bool HUD_Number::Init_Sprite()
{
	m_State_Sprite[0] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\0.png", 18, 18, 1, 1);

	m_State_Sprite[1] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\1.png", 18, 18, 1, 1);

	m_State_Sprite[2] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\2.png", 18, 18, 1, 1);

	m_State_Sprite[3] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\3.png", 18, 18, 1, 1);

	m_State_Sprite[4] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\4.png", 18, 18, 1, 1);

	m_State_Sprite[5] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\5.png", 18, 18, 1, 1);

	m_State_Sprite[6] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\6.png", 18, 18, 1, 1);

	m_State_Sprite[7] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\7.png", 18, 18, 1, 1);

	m_State_Sprite[8] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\8.png", 18, 18, 1, 1);

	m_State_Sprite[9] = new Sprite(L"Resource\\Sprite\\HUD\\Number\\9.png", 18, 18, 1, 1);

	m_sprite = *m_State_Sprite[0];

	m_State_Sprite.shrink_to_fit();

	return true;
}

