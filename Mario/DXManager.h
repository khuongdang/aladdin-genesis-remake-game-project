#pragma once
#include "System.h"

#include "GameScene.h"
#include "CutScene.h"
#include "KeyboardManager.h"

class DXManager
{
public:
	
	DXManager(HINSTANCE* hInstance, HWND* hwnd);
	~DXManager();
	bool Init();
	bool Run();
	bool End();

	static DXManager* getInstance();

	LPDIRECT3DDEVICE9* getDevice();
	HWND getHWND();
private:
	DXManager();
	static DXManager m_Instance;

	HINSTANCE* m_hInstance;
	HWND* m_hwnd;

	LPDIRECT3D9 m_d3d = NULL;
	LPDIRECT3DDEVICE9 m_d3ddv = NULL;
	LPDIRECT3DSURFACE9 m_back_buffer = NULL;
	LPDIRECT3DSURFACE9 m_surface = NULL;
};

