#include "HUD_Score.h"
#include "HUD_Desert_Number.h"
#include "HUD_Number.h"


HUD_Score::HUD_Score()
{
	display = new HUD_Desert_Number();
}


HUD_Score::~HUD_Score()
{
}

bool HUD_Score::Render(D3DXVECTOR2 vec, int amount)
{
	if (amount > 0)
	{
		vector<int> split = vector<int>(0);

		int temp = amount;
		while (temp > 0)
		{
			split.push_back(temp % 10);
			temp /= 10;
		}

		int size = split.size();
		for (int i = 0; i < size; i++)
		{
			VEC2 pos = vec;
			pos.x += 18 * (size - i);
			display->Render(pos, split[i]);
		}


	}
	else
	{
		VEC2 pos = vec;
		pos.x += 18;
		display->Render(pos, 0);
	}

	return true;
}

bool HUD_Score::Render(float x, float y, int amount)
{
	return Render(VEC2(x, y), amount);
}
