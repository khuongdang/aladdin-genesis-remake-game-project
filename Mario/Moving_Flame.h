#pragma once
#include "Projectile.h"
class Moving_Flame :
	public Projectile
{
public:
	Moving_Flame(VEC2 vec);
	~Moving_Flame();
	bool Update() override;
	int Hit();

private:
	bool Init_Sprite();
};

