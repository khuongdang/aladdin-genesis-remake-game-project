#include "FX_Pot_Explode.h"
#include "Sound.h"


FX_Pot_Explode::FX_Pot_Explode(VEC2 pos)
{
	m_TypeName = L"FX";

	m_node = Node(pos);

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_rigid_body = Rect_Update();
	Init_Sprite();

	Sound::GetInstance()->Play("Clay_Pot");
}

FX_Pot_Explode::~FX_Pot_Explode()
{
}

bool FX_Pot_Explode::Update()
{
	if (m_sprite.is_endFrame())
	{
		m_parent->Destroy(this);
		return false;
	}
	return true;
}

bool FX_Pot_Explode::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\FX\\Pot_Explode.png", 104, 39, 8, 8, 3);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
