#include "Player_Slash_Object.h"
#include "Sound.h"

Player_Slash_Object::Player_Slash_Object(VEC2 vec)
{
	m_TypeName = L"SLASH";

	m_node = Node(vec);

	m_body_size = VEC2(60, 60);

	m_rigid_body = Rect_Update();

	m_isStatic = false;

	Sound::GetInstance()->Play("High_Sword");
}

Player_Slash_Object::Player_Slash_Object(float x, float y)
{
	m_TypeName = L"SLASH";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	m_body_size = VEC2(60, 60);

	m_rigid_body = Rect_Update();

	m_isStatic = false;
}



Player_Slash_Object::~Player_Slash_Object()
{
}

bool Player_Slash_Object::Update()
{
	if (m_parent)
		m_parent->Destroy(this);
	return true;
}
