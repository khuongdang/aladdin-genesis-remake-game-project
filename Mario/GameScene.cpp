#include "GameScene.h"
#include "FXmanager.h"
#include "Sound.h"
#include "HUD_Behind_Back_Ground.h"
#include "tinyxml2.h"

#include "CutScene.h"

using namespace tinyxml2;

GameScene::GameScene() {};

GameScene::GameScene(LPDIRECT3DDEVICE9 *d3ddv ,LPDIRECT3DSURFACE9* back_buffer, LPDIRECT3DSURFACE9* surface)
{
	m_d3ddv = d3ddv;
	m_back_buffer = back_buffer;
	m_surface = surface;
	state = L"Map1";
	God_Mode = false;
	GameScene::m_Instance = *this;
}

GameScene::~GameScene()
{
	if (m_camera) { m_camera = nullptr; delete m_camera; }
	if (m_back_buffer ){ m_back_buffer = nullptr; delete m_back_buffer; }
	if (m_d3ddv) { m_d3ddv = nullptr; delete m_d3ddv; }
	if (m_surface) { m_surface = nullptr; delete m_surface; }
}

HUD_Behind_Back_Ground* hud = nullptr;
Sprite* background;
Sprite* foreground;

bool GameScene::Init()
{	
	D3DXCreateSprite(*m_d3ddv, &m_spriteHandler);
	FXmanager(this);
	Aladdin = new Player(0, 0);
	New();
	return true;
}

bool GameScene::New()
{
	m_Player_layer = QuadTree(0);
	m_Objects = QuadTree(1);
	m_Enemy = QuadTree(1);

	Just_respawn = true;

	LoadSound();
	LoadMap();

	return true;
}

bool GameScene::Run()
{
	if (Going_next_scene)
	{
		New();
		Going_next_scene = false;
	}

	if (  (*m_d3ddv)->BeginScene() )
	{
		// Clear back buffer with BLACK
		(*m_d3ddv)->ColorFill(*m_back_buffer, NULL, D3DCOLOR_XRGB(0, 0, 0));

		// Fill the bitmap
		

		// Check and Update All Objects
		Update_Object();
		 //Draw the surface onto the back buffer
		//(*m_d3ddv)->StretchRect(
		//	*m_surface,			// from 
		//	NULL,				// which portion?
		//	*m_back_buffer,		// to 
		//	&rect,				// which portion?
		//	D3DTEXF_NONE);
		

		Render();
		Camera::getInstance()->Update();
		Camera::getInstance()->Render();

		(*m_d3ddv)->EndScene();
	}

	(*m_d3ddv)->Present(NULL, NULL, NULL, NULL);

	return false;
}

bool colided = false;

bool GameScene::Update_Object()
{
	//(*m_d3ddv)->StretchRect(
	//	*m_surface,			// from 
	//	NULL,				// which portion?
	//	*m_back_buffer,		// to 
	//	&ground,				// which portion?
	//	D3DTEXF_NONE);

	if (God_Mode)
	{
		Aladdin->Set_Apple_Amount(999);
		Aladdin->Set_Healh(8);
	}

	if (Just_respawn)
	{
		if(state == L"Map1")
			Sound::GetInstance()->Play("ThemeMap1", true);
		else if (state == L"Boss")
			Sound::GetInstance()->Play("ThemeBoss", true);
		Just_respawn = false;
	}

	m_Player_layer.Update();
	m_Objects.Update();
	m_Enemy.Update();

	vector<Object*> player_layer = m_Player_layer.getObject();
	vector<Object*> Objects = m_Objects.getObject();
	vector<Object*> Enemys = m_Enemy.getObject();

	// Check if the scene need to spawn apple
	if (!Aladdin->Get_apple_amount())
	{
		if (!Spawn_apple)
		{
			Spawn_apple = true;
			for (int i = 0; i < Objects.size(); i++)
			{
				if (Objects[i]->m_TypeName == L"SPAWN_APPLE")
				{
					Spawn_Apple* spawn = ((Spawn_Apple*)Objects[i]);

					VEC2 Ala_pos = Aladdin->get_Position();
					float mid = background->get_width() / 2;

					int iside = 0;
					if (Ala_pos.x >= mid)
						iside = FACE_LEFT;
					else
						iside = FACE_RIGHT;
					if (spawn->side == iside)
					{
						VEC2 pos = spawn->get_Position();
						m_Objects.Add(new Item_Apple(pos.x, pos.y));
					}
				}
			}
		}
	}
	else
		Spawn_apple = false;
	
	// PLAYER vs OBJECT
	for (int j = 0; j < player_layer.size(); j++)
	{
		colided = false;
		//check colision
		for (int i = 0; i < Objects.size(); i++)
		{			
			if (Math::isCollided(player_layer[j]->get_Rect(), Objects[i]->get_Rect()))
			{
				if (player_layer[j]->m_TypeName == L"APPLE")
				{
					if (Objects[i]->m_TypeName == L"OBSTACLE")
					{
						player_layer[j]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"FLOATGROUND")
					{
						player_layer[j]->Hit();
					}
						
				}
				else if (player_layer[j]->m_TypeName == L"SLASH")
				{
					if (Objects[i]->m_TypeName == L"ITEM_CHECKPOINT")
					{
						Objects[i]->Hit();
					}
				}
				else if (player_layer[j]->m_TypeName == L"PLAYER")
				{
					if (Objects[i]->m_TypeName == L"OBSTACLE")
					{
						Colided(player_layer[j], Objects[i]);
					}
					else if (Objects[i]->m_TypeName == L"FLOATGROUND")
					{
						Colided(player_layer[j], Objects[i]);
						((FloatGround*)Objects[i])->Drop();
					}
					else if (Objects[i]->m_TypeName == L"BAR")
					{
						if (!Aladdin->is_onCountdown_Hang())
							Hanging(player_layer[j], Objects[i]);
					}
					else if (Objects[i]->m_TypeName == L"ROPE")
					{
						if (!Aladdin->is_onCountdown_Climb() && !Aladdin->isHanging() )
							Climbing(player_layer[j], Objects[i]);
					}
					else if (Objects[i]->m_TypeName == L"CAMEL")
					{
						Bounc(player_layer[j], Objects[i]);
					}
					else if (Objects[i]->m_TypeName == L"SPRINGBOARD")
					{
						Bounc(player_layer[j], Objects[i], STATE_JUMP_SPRING);
					}
					else if (Objects[i]->m_TypeName == L"ITEM_APPLE")
					{
						((Player*)player_layer[j])->Add_apple();
						Objects[i]->Hit();
					}
					else if(Objects[i]->m_TypeName == L"TRIGGER")
					{
						Trigger* trigger = ((Trigger*)Objects[i]);
						if (trigger->m_SubType == L"NEXTSCENE")
						{
							CutScene::get_Instance()->New(L"WIN");
							next_Scene(L"Boss");
						}
						if (trigger->m_SubType == L"STAIR")
						{
							if (trigger->countdown <= 0)
							{
								//Toggle active state of all stairs with the right subindex 
								for (int u = 0; u < Objects.size(); u++)
								{
									if (Objects[u]->m_TypeName == L"STAIR")
									{
										Stair_With_Trigger* stair = ((Stair_With_Trigger*)Objects[u]);
										if (stair->m_SubIndex == trigger->m_SubIndex)
										{
											stair->m_state = !stair->m_state;
										}
									}
								}
								trigger->countdown = 15;
							}
							else
							{
								// wair for player to step out
								trigger->countdown = 15;
							}
							

						}

					}
					else if (Objects[i]->m_TypeName == L"STAIR")
					{
						if (((Stair_With_Trigger*)Objects[i])->m_state)
						{
							Colided(player_layer[j], Objects[i]);
						}
					}
					else if (Objects[i]->m_TypeName == L"PLATFORM")
					{
						Colided_Up(player_layer[j], Objects[i]);
					}	
					else if (Objects[i]->m_TypeName == L"COAL")
					{
						VEC2 pos = player_layer[j]->get_Position();
						((Coal*)Objects[i])->Create_Fire(pos);
					}
					else if (Objects[i]->m_TypeName == L"FIRE")
					{
						player_layer[j]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"ITEM_HEALTH")
					{
						((Player*)player_layer[j])->Extra_Health();
						Objects[i]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"ITEM_CHECKPOINT")
					{
						CheckPoint = Objects[i]->get_Position();
						CheckPoint.y -= 20;
						Objects[i]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"ITEM_RUBY")
					{
						((Player*)player_layer[j])->Add_ruby();
						Objects[i]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"ITEM_GENIE")
					{
						Objects[i]->Hit();
					}
					else if (Objects[i]->m_TypeName == L"ITEM_MONKEY")
					{
						Objects[i]->Hit();
					}

					

					
				}
			}		
		}

		if (colided == false && ((GroundObject*)player_layer[j])->is_Grounded())
			((GroundObject*)player_layer[j])->set_Grounded(false);
		else if(colided == true)
			((GroundObject*)player_layer[j])->set_Grounded(true);
	
	}

	// ENEMY vs OBJECT
	for (int j = 0; j < Enemys.size(); j++)
	{
		colided = false;
		//check colision
		for (int i = 0; i < Objects.size(); i++)
		{
			
				if (Math::isCollided(Enemys[j]->get_Rect(), Objects[i]->get_Rect()))
				{
					if (Enemys[j]->m_TypeName == L"ENEMY")
					{
						if (Objects[i]->m_TypeName == L"OBSTACLE")
						{
							Colided(Enemys[j], Objects[i]);
						}
					}
					else if (Enemys[j]->m_TypeName == L"KNIFE")
					{
						if (Objects[i]->m_TypeName == L"OBSTACLE" || Objects[i]->m_TypeName == L"STAIR")
						{
							Enemys[j]->Hit();
						}
					}
					else if (Enemys[j]->m_TypeName == L"SPELL")
					{
						if (Objects[i]->m_TypeName == L"OBSTACLE")
						{
							Enemys[j]->Hit();
						}
					}
					else if (Enemys[j]->m_TypeName == L"MOVING_FLAME")
					{
						if (Objects[i]->m_TypeName == L"OBSTACLE")
						{
							Enemys[j]->Hit();
						}
					}
				}
			
		}

		if (colided == false && ((GroundObject*)Enemys[j])->is_Grounded())
			((GroundObject*)Enemys[j])->set_Grounded(false);
		else if (colided == true)
			((GroundObject*)Enemys[j])->set_Grounded(true);

	}

	// PLAYER vs ENEMY
	for (int j = 0; j < player_layer.size(); j++)
	{
		for (int i = 0; i < Enemys.size(); i++)
		{
			if (Math::isCollided(player_layer[j]->get_Rect(), Enemys[i]->get_Rect()))
			{
				if (player_layer[j]->m_TypeName == L"PLAYER")
				{
					if (Enemys[i]->m_TypeName == L"KNIFE")
					{
						player_layer[j]->Hit();
						Enemys[i]->Hit();
					}
					else if (Enemys[i]->m_TypeName == L"SPELL")
					{
						float x = Enemys[i]->get_Velocity().x;
						VEC2 pull = VEC2(0, 0 );
						if (x > 0)
							pull.x = -3;
						else if (x < 0)
							pull.x = 3;
						player_layer[j]->Translate(pull);
						Enemys[i]->Hit();
					}
					else if (Enemys[i]->m_TypeName == L"ENEMY_SLASH")
					{
						player_layer[j]->Hit();
					}
					else if (Enemys[i]->m_TypeName == L"MOVING_FLAME")
					{
						player_layer[j]->Hit();
					}
					else if (Enemys[i]->m_TypeName == L"BOSS")
					{
						player_layer[j]->Hit();
					}
				}
				else if (player_layer[j]->m_TypeName == L"APPLE")
				{
					if (Enemys[i]->m_TypeName == L"ENEMY" )
					{
						player_layer[j]->Hit();
						Enemys[i]->Hit();
					}
					else if (Enemys[i]->m_TypeName == L"BOSS")
					{
						player_layer[j]->Hit();
						Enemys[i]->Hit();
					}
					
				}
				else if (player_layer[j]->m_TypeName == L"SLASH")
				{
					if (Enemys[i]->m_TypeName == L"ENEMY")
					{
						Enemys[i]->Hit();
					}
				}
			}
		}
	}

	return true;

}

bool GameScene::Render() {
	float h = background->get_height() / 2;
	float w = background->get_width() / 2;

	hud->Render();

	background->Render(w, h, FACE_RIGHT);

	m_Objects.Render();
	m_Enemy.Render();
	m_Player_layer.Render();
	foreground->Render(w, h, FACE_RIGHT);
	
	return true;
}

bool GameScene::Colided(Object* a, Object* b)
{
	// check if a is movable object

	RECT rect_a = a->get_Rect();
	RECT rect_b = b->get_Rect();

	VEC2 vec_a = a->get_Velocity();
	VEC2 normal;
	float normalx = 0;
	float normaly = 0;

	float a_width = rect_a.right - rect_a.left;
	float a_height = rect_a.bottom - rect_a.top;

	float b_width = rect_b.right - rect_b.left;
	float b_height = rect_b.bottom - rect_b.top;

	float totalX = min(a_width, b_width);
	float totalY = min(a_height, b_height);

	bool newColided = true;

		if (rect_a.right >= rect_b.left && rect_a.right <= rect_b.right)
		{
			normalx = (rect_b.left - rect_a.right - 1) ; //move backward
			if (normalx < -totalX - 1)
			{
				normalx = -totalX - 1;
			}
		}
		else	
		if (rect_a.left <= rect_b.right)
		{
			normalx = (rect_b.right - rect_a.left + 1);	//move forward

			if (normalx > totalX + 1)
			{
				normalx = totalX + 1;
			}
		}

		if (rect_a.bottom >= rect_b.top && rect_a.bottom <= rect_b.bottom)
		{	
				normaly = (rect_b.top - rect_a.bottom);

				if (normaly < -totalY)
				{
					newColided = false;
					normaly = -totalY;
				}
		}else
		if (rect_a.top <= rect_b.bottom)
		{
				newColided = false;
				normaly = (rect_b.bottom - rect_a.top);

				if (normaly > totalY)
				{
					normaly = totalY;
				}
		}

	if ( (Math::abs(normalx) / totalX) < (Math::abs(normaly) / totalY))
	{
		normal.x = normalx;
		normal.y = 0;
	}
	else
	{
		normal.x = 0;
		normal.y = normaly;
	}

	a->Translate(normal);
	
	if (colided == false)
		colided = newColided;
	
	return true;
}

bool GameScene::Colided_Up(Object* a, Object* b)
{

	RECT rect_a = a->get_Rect();
	RECT rect_b = b->get_Rect();

	VEC2 normal;
	float normalx = 0;
	float normaly = 0;

	float a_height = rect_a.bottom - rect_a.top;
	float b_height = rect_b.bottom - rect_b.top;

	float totalY = min(a_height, b_height);

	bool newColided = true;

	if (rect_a.bottom >= rect_b.top && rect_a.bottom <= rect_b.bottom)
	{
		normaly = (rect_b.top - rect_a.bottom);

		if (normaly < -totalY)
		{
			newColided = false;
			normaly = -totalY;
		}
	}
	else if (rect_a.top <= rect_b.bottom)
	{
		newColided = false;
		normaly = 0;
	}

	normal.x = 0;
	normal.y = normaly;

	a->Translate(normal);

	if (colided == false)
		colided = newColided;

	return true;
}

bool GameScene::Bounc(Object* a, Object* b, int type)
{

	RECT rect_a = a->get_Rect();
	RECT rect_b = b->get_Rect();


	if (!((Player*)a)->is_Grounded() && a->get_Velocity().y > 0)
	{
		((Player*)a)->set_Grounded(true);
		((Player*)a)->Jump(type);
		b->Hit();
	}
		
	return true;

}

bool GameScene::Hanging(Object* a, Object* b)
{
	RECT rect_a = a->get_Rect();
	RECT rect_b = b->get_Rect();

	float x = a->get_Position().x;
	float y = b->get_Position().y + (rect_a.bottom - rect_a.top)/2 - 5;
	a->set_Position(x, y);
	Aladdin->Hang((HorizontalBar*)b);

	return true;
}

bool GameScene::Climbing(Object* a, Object* b)
{
	RECT rect_a = a->get_Rect();
	RECT rect_b = b->get_Rect();

	float x = b->get_Position().x;
	float y = a->get_Position().y;
	a->set_Position(x, y);
	((Player*)a)->Climb((Rope*)b);

	return true;
}

bool GameScene::LoadMap()
{
	hud = new HUD_Behind_Back_Ground();

	tinyxml2::XMLDocument doc;

	RECT mapRect = RECT();
	mapRect.top = 0;
	mapRect.left = 0;

	if (state == L"Map1")
	{
		background = new Sprite(L"Resource\\Map\\Map1\\Background.png", 4773, 689, 1, 1);
		foreground = new Sprite(L"Resource\\Map\\Map1\\Foreground.png", 4773, 689, 1, 1);

		mapRect.bottom = 689;
		mapRect.right = 4773;
		doc.LoadFile("Resource\\Map\\Map1\\Objects.xml");

		Sprite *temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\1.png", 512, 313, 1, 1);
		hud->add_sprite(temp);
		temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\2.png", 512, 313, 1, 1);
		hud->add_sprite(temp);
		temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\3.png", 512, 313, 1, 1);
		hud->add_sprite(temp);
		temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\4.png", 512, 313, 1, 1);
		hud->add_sprite(temp);
		temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\5.png", 512, 313, 1, 1);
		hud->add_sprite(temp);
		temp = new Sprite(L"Resource\\Map\\Map1\\Desert\\6.png", 512, 313, 1, 1);
		hud->add_sprite(temp);

		temp = nullptr;
		delete temp;
	}
	else if (state == L"Boss")
	{
		background = new Sprite(L"Resource\\Map\\Boss\\Background.png", 752, 408, 1, 1);
		foreground = new Sprite(L"Resource\\Map\\Boss\\Foreground.png", 752, 408, 1, 1);

		mapRect.bottom = 408;
		mapRect.right = 752;
		Sound::GetInstance()->Stop();
		doc.LoadFile("Resource\\Map\\Boss\\Objects.xml");

		Sprite *temp = new Sprite(L"Resource\\Map\\Boss\\Pillar.png", 512, 271, 1, 1);
		hud->add_sprite(temp);
	}


	m_Objects.Set_bound(mapRect);
	m_Player_layer.Set_bound(mapRect);
	m_Enemy.Set_bound(mapRect);

	tinyxml2::XMLElement* root = doc.FirstChildElement();
	tinyxml2::XMLElement* group = nullptr;
	
	for (group = root->FirstChildElement(); group != NULL; group = group->NextSiblingElement())
	{
		float x = 0;
		float y = 0;
		float h = 0;
		float w = 0;
		float x_g = 0;
		float y_g = 0;
		int id = 0;


		tinyxml2::XMLElement* object = nullptr;
		for (object = group->FirstChildElement(); object != NULL; object = object->NextSiblingElement())
		{

			object->QueryFloatAttribute("x", &x);
			object->QueryFloatAttribute("y", &y);
			object->QueryFloatAttribute("width", &w);
			object->QueryFloatAttribute("height", &h);

			x_g = x + w / 2;
			y_g = y + h / 2;
				
			x = x_g;
			y = y - h / 2;

			const char* type = group->Attribute("name");

			if (std::strcmp(type, "Player") == 0)
			{
				Aladdin->set_Position(x, y);

				m_Player_layer.Add(Aladdin);

				Camera(m_d3ddv, Aladdin, mapRect);

				m_camera = Camera::getInstance();

				hud->set_cam(m_camera);

				CheckPoint = VEC2(x, y);
			}
			else if (std::strcmp(type, "Ground") == 0 || std::strcmp(type, "Wall") == 0)
			{		
				m_Objects.Add(new Wall(x_g, y_g, w, h));
			}
			else if (std::strcmp(type, "Enemy_1") == 0)
			{
				m_Enemy.Add(new Hakim(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Enemy_2") == 0)
			{
				m_Enemy.Add(new Nahbi(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Enemy_3") == 0)
			{
				m_Enemy.Add(new Fazal(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Enemy_4_Left") == 0 || std::strcmp(type, "Enemy_4_Right") == 0)
			{
				m_Enemy.Add(new KnifeGuy(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Enemy_5") == 0)
			{
				m_Enemy.Add(new HidePot(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Enemy_6") == 0)
			{
				m_Enemy.Add(new ThrowPot(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Rope") == 0)
			{
				m_Objects.Add(new Rope(x_g,y_g, w, h));
			}
			else if (std::strcmp(type, "FloatGround") == 0)
			{
				m_Objects.Add(new FloatGround(x, y));
			}
			else if (std::strcmp(type, "HorizontalBar") == 0)
			{
				m_Objects.Add(new HorizontalBar(x_g, y_g, w, h));
			}
			else if (std::strcmp(type, "Camel") == 0)
			{
				m_Objects.Add(new Camel(x, y));
			}
			else if (std::strcmp(type, "Springboard") == 0)
			{
				m_Objects.Add(new SpringBoard(x, y));
			}
			else if (std::strcmp(type, "Coal") == 0)
			{
				m_Objects.Add(new Coal(x, y,w,h));
			}
			else if (std::strcmp(type, "Apple") == 0)
			{
				m_Objects.Add(new Item_Apple(x, y));
			}
			else if (std::strcmp(type, "Stair") == 0)
			{
				Stair_With_Trigger* stair = new Stair_With_Trigger(x_g, y_g, w, h);
				int type = group->IntAttribute("type");
				stair->m_SubIndex = type;
				m_Objects.Add(stair);
			}
			else if (std::strcmp(type, "Trigger") == 0)
			{
				Trigger* trigger = new Trigger(x_g, y_g, w, h);
				int index = group->IntAttribute("type");
				trigger->m_SubType = L"STAIR";
				trigger->m_SubIndex = index;
				m_Objects.Add(trigger);
			}
			else if (std::strcmp(type, "ToJafarScene") == 0)
			{
				Trigger* trigger = new Trigger(x_g, y_g, w, h);
				trigger->m_SubType = L"NEXTSCENE";
				m_Objects.Add(trigger);
			}
			else if (std::strcmp(type, "Platform") == 0)
			{
				Wall* wall = new Wall(x_g, y_g, w, h);
				wall->m_TypeName = L"PLATFORM";
				m_Objects.Add(wall);
			}
			else if (std::strcmp(type, "Jafar") == 0)
			{
				m_Enemy.Add(new Jafar(x, y, Aladdin));
			}
			else if (std::strcmp(type, "Health") == 0)
			{
				m_Objects.Add(new Item_Health(VEC2(x, y)));
			}
			else if (std::strcmp(type, "Checkpoint") == 0)
			{
				m_Objects.Add(new Item_Checkpoint(VEC2(x, y)));
			}
			else if (std::strcmp(type, "Gem") == 0)
			{
				m_Objects.Add(new Item_Ruby(VEC2(x, y)));
			}
			else if (std::strcmp(type, "Spawn_Apple") == 0)
			{
				const char* side = group->Attribute("side");
				Spawn_Apple* spawn = new Spawn_Apple(VEC2(x, y));
				if (std::strcmp(side, "Left") == 0)
					spawn->side = FACE_LEFT;
				else if (std::strcmp(side, "Right") == 0)
					spawn->side = FACE_RIGHT;
				m_Objects.Add(spawn);
			}
			else if (std::strcmp(type, "Genie") == 0)
			{
				m_Objects.Add(new Item_Genie(VEC2(x, y)));
			}
			else if (std::strcmp(type, "Monkey") == 0)
			{
				m_Objects.Add(new Item_Monkey(VEC2(x, y)));
			}
		}

	}


	//m_Enemy.Add(new Hakim(600, 500, Aladdin));
	//m_Enemy.Add(new Nahbi(500, 500, Aladdin));
	//m_Enemy.Add(new Fazal(700, 500, Aladdin));


	//m_Enemy.Add(new KnifeGuy(900,350,Aladdin));
	//for(int i =0; i < 70;i++)
	//m_Enemy.Add(new ThrowPot(500 , 350, Aladdin));
	//m_Enemy.Add(new HidePot(400, 350, Aladdin));



	//m_Objects.Add(new Wall(4773 / 2, 650, 4773, 50));	// wall at the bottom of the map
	//m_Objects.Add(new Wall(25, 689 / 2, 50, 800));		// wall at the left of the map

	return true;
}

bool GameScene::LoadSound()
{
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Map1\\Theme.wav", "ThemeMap1");
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Boss\\Theme.wav", "ThemeBoss");
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Apple_Splat.wav", "Apple_Splat"); // tao bi be
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Aladdin_Oof.wav", "Aladdin_Oof"); // Ohpppp
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Aladdin_Hurt.wav", "Aladdin_Hurt"); // bi dau
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Apple_Collect.wav", "Apple_Collect"); // an tien,apple
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Camel_Spit.wav", "Camel_Spit"); // dung lac da
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Clay_Pot.wav", "Clay_Pot"); // quang chen
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\High Sword.wav", "High_Sword"); // Chem
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Object Throw.wav", "Object_Throw"); // Choi
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Cloud Poof.wav", "Cloud Poof"); // Choi
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Fire From Coal.wav", "Fire From Coal"); // Lua tu than
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Canopy Bounce.wav", "Canopy Bounce"); //Springboard bounce
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Extra Health.wav", "Extra Health"); // th�m m�u
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Continue Point.wav", "Continue Point"); // checkpoint
	Sound::GetInstance()->LoadSound("Resource\\Sounds\\Gem Collect.wav", "Gem Collect"); //Ruby
	return true;
}

bool GameScene::End()
{
	return false;
}

bool GameScene::Up()
{
	Aladdin->Up();

	return true;
}

bool GameScene::Down()
{
	Aladdin->Down();
	return false;
}

bool GameScene::Left()
{
	Aladdin->Move_Left();
	return true;
}

bool GameScene::Right()
{
	Aladdin->Move_Right();
	return false;
}

bool GameScene::Stand()
{
	Aladdin->Stand();
	return true;
}

bool GameScene::X()
{	
	if ( Aladdin->Slash())
	{
		VEC2 pos = Aladdin->get_Position();
		pos.x += Aladdin->get_facing() * 30;
		m_Player_layer.Add(new Player_Slash_Object(pos));
		return true;
	}
	 // Chem
	return false;
}

bool GameScene::Z()
{
	return Aladdin->Jump();
}

bool GameScene::A()
{
	if(Aladdin->Get_apple_amount() > 0)
		Aladdin->Throw();
	return false;
}

bool GameScene::next_Scene(LPWSTR scene)
{
	Going_next_scene = true;
	state = scene;
	Sound::GetInstance()->Stop("Theme");
	return true;
}

bool GameScene::Add_Score(int score)
{
	Aladdin->Add_score(score);
	return true;
}

GameScene* GameScene::getInstance()
{
	return &GameScene::m_Instance;
}

LPD3DXSPRITE* GameScene::getSpriteHandler()
{
	return &m_spriteHandler;
}

Apple* GameScene::CreateApple(VEC2 pos, VEC2 velocity, VEC2 gravity)
{
	Apple* apple = new Apple(pos.x,pos.y);

	apple->set_Velocity(velocity);
	apple->set_Gravity(gravity);
	m_Player_layer.Add(apple);
	return	apple;
}

Knife* GameScene::CreateKnife(VEC2 pos, VEC2 velocity, VEC2 gravity)
{
	Knife* knife = new Knife(pos.x, pos.y);

	knife->set_Velocity(velocity);
	knife->set_Gravity(gravity);
	m_Enemy.Add(knife);
	return	knife;
}

Pot* GameScene::CreatePot(VEC2 pos, VEC2 velocity, VEC2 gravity)
{
	Pot* pot = new Pot(pos.x, pos.y);

	pot->set_Velocity(velocity);
	pot->set_Gravity(gravity);
	m_Enemy.Add(pot);
	return	pot;
}

Spell * GameScene::CreateSpell(VEC2 pos, VEC2 velocity, VEC2 gravity)
{
	Spell* spell = new Spell(pos.x, pos.y);

	spell->set_Velocity(velocity);
	spell->set_Gravity(gravity);
	m_Enemy.Add(spell);
	return	spell;
}

Object * GameScene::Add_Object(Object* object)
{
	m_Objects.Add(object);
	return object;
}

Object * GameScene::Add_Player_layer(Object* object)
{
	m_Player_layer.Add(object);
	return object;
}

Enemy_Slash_Object * GameScene::Enemy_Slash(VEC2 pos)
{
	Enemy_Slash_Object* slash = new Enemy_Slash_Object(pos);

	m_Enemy.Add(slash);
	return slash;
}

Moving_Flame * GameScene::Create_Moving_Flame(VEC2 pos, VEC2 vel)
{
	Moving_Flame* flame = new Moving_Flame(pos);
	flame->set_Velocity(vel);
	m_Enemy.Add(flame);
	return flame;
}

Object* GameScene::CreateFX(Object* FX)
{
	m_Objects.Add(FX);
	return FX;
}

bool  GameScene::Respawn()
{
	CutScene::get_Instance()->New(L"DEAD");
	Aladdin->set_Position(CheckPoint.x, CheckPoint.y-20);
	Just_respawn = true;
	return true;
}

bool GameScene::Init_Snake_Boss_State()
{
	vector<Object*> Objects = m_Objects.getObject();
	for (int i = 0; i < Objects.size(); i++)
	{
		Object* object = Objects[i];
		if (Objects[i]->m_TypeName == L"PLATFORM")
		{
			VEC2 pos = object->get_Position();
			pos.y = object->get_Rect().top - 20;
			float widght = object->get_Rect().right - object->get_Rect().left-10;
			Coal* coal = new Coal(pos.x, pos.y, widght, 10);
			m_Objects.Add(coal);
		}
	}

	return false;
}

bool GameScene::Toggle_God_Mode()
{
	God_Mode = !God_Mode;
	return God_Mode;
}

GameScene GameScene::m_Instance;
