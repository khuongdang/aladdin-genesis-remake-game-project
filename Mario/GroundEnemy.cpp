#include "GroundEnemy.h"
#include "FXmanager.h"


GroundEnemy::GroundEnemy()
{
}


GroundEnemy::~GroundEnemy()
{
}

int GroundEnemy::Hit()
{
	m_heath--;
	if (State_Sprite.size() > 1 && State_Sprite[STATE_STUNT] != nullptr  )
	{
		m_Blocking_Sprite = false;
		Change_State(STATE_STUNT, true);
		m_node.set_Accelerate(0, 0);
		m_node.set_Velocity(0, 0);
	}
	if (is_Death())
	{
		m_parent->Destroy(this);
		FXmanager::get_instance()->CreateFX(L"Explode_On_Dead", m_node.get_Position());		
	}
	return m_heath;
}

bool GroundEnemy::is_Death()
{
	if (m_heath <= 0)
		return true;
	return false;
}
