#include "FX_Explode_On_Dead.h"
#include "Sound.h"

FX_Explode_On_Dead::FX_Explode_On_Dead(VEC2 pos)
{
	m_TypeName = L"FX";

	m_node = Node(pos);

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_rigid_body = Rect_Update();
	Init_Sprite();

	Sound::GetInstance()->Play("Cloud Poof");
}


FX_Explode_On_Dead::~FX_Explode_On_Dead()
{
}

bool FX_Explode_On_Dead::Update()
{
	if (m_sprite.is_endFrame())
	{
		m_parent->Destroy(this);
		return false;
	}
	return true;
}


bool FX_Explode_On_Dead::Init_Sprite()
{

	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\FX\\Explode_On_Dead.png", 120, 120, 8, 4,3);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}