#pragma once
#include "Object.h"
class Item_Checkpoint :
	public Object
{
public:
	Item_Checkpoint(VEC2 pos);
	~Item_Checkpoint();

	bool Update() override;

	bool Init_Sprite();
	int Hit();

	bool is_activated = false;
};

