#pragma once
#include "GroundEnemy.h"

class Fazal :
	public GroundEnemy
{
public:
	Fazal(int X, int Y, Player* Following = nullptr);
	~Fazal();

	bool Update() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Throw();

	bool Stand();

private:

	
	bool Init_Sprite() override;

};

