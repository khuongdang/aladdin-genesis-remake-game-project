#pragma once

#include "System.h"

class Node
{
public:
	Node(D3DXVECTOR2 Position = D3DXVECTOR2(0, 0),
		D3DXVECTOR2 Gravity = D3DXVECTOR2(0, 0),
		D3DXVECTOR2 Velocity = D3DXVECTOR2(0, 0),
		D3DXVECTOR2 Accelerate = D3DXVECTOR2(0, 0),
		float maxspeed = 0,
		float friction = 0,
		float maxfalling = 0
	);

	~Node();

	D3DXVECTOR2 set_Position(D3DXVECTOR2 Position);
	D3DXVECTOR2 set_Position(float x, float y);

	VEC2 Translate(VEC2 vec);

	VEC2 Translate(float x, float y);

	D3DXVECTOR2 set_Gravity(D3DXVECTOR2 Gravity);
	D3DXVECTOR2 set_Gravity( float x , float y );

	D3DXVECTOR2 set_Velocity(D3DXVECTOR2 Velocity);
	D3DXVECTOR2 set_Velocity(float x, float y);

	D3DXVECTOR2 set_Accelerate(D3DXVECTOR2 Accelerate);
	D3DXVECTOR2 set_Accelerate(float x, float y);

	D3DXVECTOR2 get_Position();
	float x();
	float y();

	D3DXVECTOR2 get_Gravity();
	float get_Gravity_X();
	float get_Gravity_Y();

	D3DXVECTOR2 get_Velocity();
	float get_Velocity_X();
	float get_Velocity_Y();


	D3DXVECTOR2 get_Accelerate();
	float get_Accelerate_X();
	float get_Accelerate_Y();

	float Set_maxSpeed(float maxspeed);
	float set_maxFallingSpeed(float param);
	float get_maxFallingSpeed();
	float Get_maxSpeed();

	float Set_friction(float friction);
	float Get_friction();

	bool Update();
	
private:
	D3DXVECTOR2 m_Position;
	D3DXVECTOR2 m_Gravity;
	D3DXVECTOR2 m_Velocity;
	D3DXVECTOR2 m_Accelerate;

	float m_maxspeed;
	float m_friction;
	float m_maxfallingSpeed;
};

