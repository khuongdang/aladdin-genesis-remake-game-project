#include "FloatGround.h"



FloatGround::FloatGround(float x, float y)
{
	m_TypeName = L"FLOATGROUND";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);
	m_origin_pos = pos;

	m_state = STATE_IDLE;
	m_facing = FACE_RIGHT;

	m_body_size = VEC2(32, 16);

	m_rigid_body = Rect_Update();

	m_isStatic = true;
	m_before_drop = 15;

	Init_Sprite();
}

bool FloatGround::Drop()
{
	touched = true;
	m_countdown = 20;
	return true;
}

bool FloatGround::Update()
{
	if (touched)
	{
		if (m_before_drop <= 15 && m_before_drop > 0)
			m_before_drop--;
		else if (m_before_drop <= 0)
		{
			

			if (m_countdown > 0)
			{
				m_countdown--;
				m_node.set_Gravity(GRAVITY);
			}
			else 
			{
				m_node.set_Gravity(0, 0);
				m_node.set_Velocity(0, 0);
				m_node.set_Position(m_origin_pos);
				m_countdown = 20;
				m_before_drop = 15;
				touched = false;
			}	
		}
	}
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}


FloatGround::~FloatGround()
{
}

bool FloatGround::Init_Sprite()
{

	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\FloatGround\\Idle.png", 32, 16, 1, 1);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}
