#pragma once
#include "Projectile.h"
class Saliva :
	public Projectile
{
public:
	Saliva(VEC2 pos);
	~Saliva();

	bool Update() override;
	int Hit();
private:
	bool Init_Sprite() override;
	
};

