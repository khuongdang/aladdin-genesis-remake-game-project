#include "Player.h"
#include "Sound.h"
#include "Camera.h"
#include "GameScene.h"


Player::Player(int X, int Y)
{
	m_TypeName = L"PLAYER";

	VEC2 pos = VEC2(X,Y);
	m_node = Node(pos,GRAVITY);

	Init_Sprite();

	m_node.Set_maxSpeed(PLAYER_MAX_SPEED);
	m_node.Set_friction(PLAYER_FRICTION);
	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size =VEC2(PLAYER_WIDGHT, PLAYER_HEIGHT);
	m_anchor_point = VEC2(0, 20);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_isClimbing = false;
	m_isGrounded = false;
	m_isStatic = false;

	Reset_Stat();
}


Player::~Player()
{
	for (int i = 0; i < State_Sprite.size(); i++)
		delete State_Sprite[i];
}

bool Player::Init_Sprite()
{


	State_Sprite.resize(25);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Aladdin\\Idle.png", 60,PLAYER_SPRITE_HEIGHT,
																									16,
																									8,
																									5);
	State_Sprite[STATE_STUNT] = new Sprite(L"Resource\\Sprite\\Aladdin\\Stunted.png", 100, PLAYER_SPRITE_HEIGHT,6,	6,	5);


	State_Sprite[STATE_RUN] = new Sprite(L"Resource\\Sprite\\Aladdin\\Run.png", 60,
																								PLAYER_SPRITE_HEIGHT,
																								13,
																								13, 5, 2);

	State_Sprite[STATE_DUCK] = new Sprite(L"Resource\\Sprite\\Aladdin\\Duck.png", 60,
																									PLAYER_SPRITE_HEIGHT,
																									4,
																									4,
																									3,
																									3);

	State_Sprite[STATE_RUN_JUMP] = new Sprite(L"Resource\\Sprite\\Aladdin\\Run_Jump.png", 80,
																											PLAYER_SPRITE_HEIGHT,
																											8, 8, 5, 7);

	State_Sprite[STATE_SLASH] = new Sprite(L"Resource\\Sprite\\Aladdin\\Slash.png", 120,PLAYER_SPRITE_HEIGHT,
																						5, 5, 3,0,3);

	State_Sprite[STATE_LOOKUP] = new Sprite(L"Resource\\Sprite\\Aladdin\\LookUp.png",60,PLAYER_SPRITE_HEIGHT,
		3, 3, 5, 2);

	State_Sprite[STATE_THROW] = new Sprite(L"Resource\\Sprite\\Aladdin\\Throw.png", 60, PLAYER_SPRITE_HEIGHT, 6, 6, 3,0,3);

	State_Sprite[STATE_THROW_DUCK] = new Sprite(L"Resource\\Sprite\\Aladdin\\Throw_Duck.png", 120, PLAYER_SPRITE_HEIGHT, 5, 5, 5,0,3);

	State_Sprite[STATE_SLASH_DUCK] = new Sprite(L"Resource\\Sprite\\Aladdin\\Slash_Duck.png", 150, PLAYER_SPRITE_HEIGHT, 8, 4, 3,0,5);

	State_Sprite[STATE_THROW_JUMP] = new Sprite(L"Resource\\Sprite\\Aladdin\\Throw_Jump.png", 60, PLAYER_SPRITE_HEIGHT, 5, 5, 5,0,3);

	State_Sprite[STATE_SLASH_JUMP] = new Sprite(L"Resource\\Sprite\\Aladdin\\Slash_Jump.png", 140, PLAYER_SPRITE_HEIGHT, 6, 3, 3,0,3);

	State_Sprite[STATE_CLIMB] = new Sprite(L"Resource\\Sprite\\Aladdin\\Idle_Vertical.png", 40, PLAYER_SPRITE_HEIGHT, 1, 1, 5);

	State_Sprite[STATE_CLIMB_UP] = new Sprite(L"Resource\\Sprite\\Aladdin\\Climb_Up.png", 40, PLAYER_SPRITE_HEIGHT, 10, 10, 5);

	State_Sprite[STATE_CLIMB_DOWN] = new Sprite(L"Resource\\Sprite\\Aladdin\\Climb_Down.png", 40, PLAYER_SPRITE_HEIGHT, 10, 10, 5);

	State_Sprite[STATE_HANG] = new Sprite(L"Resource\\Sprite\\Aladdin\\Hang_Idle.png", 60, PLAYER_SPRITE_HEIGHT, 5, 5, 10);

	State_Sprite[STATE_HANG_RUN] = new Sprite(L"Resource\\Sprite\\Aladdin\\Hang_Run.png", 100, PLAYER_SPRITE_HEIGHT, 10, 10, 6);

	State_Sprite[STATE_THROW_HANG] = new Sprite(L"Resource\\Sprite\\Aladdin\\Hang_Throw.png", 70, PLAYER_SPRITE_HEIGHT, 5, 5, 5,0,3);

	State_Sprite[STATE_SLASH_HANG] = new Sprite(L"Resource\\Sprite\\Aladdin\\Hang_Slash.png", 120, PLAYER_SPRITE_HEIGHT, 7, 7, 6,0,3);

	State_Sprite[STATE_JUMP_SPRING] = new Sprite(L"Resource\\Sprite\\Aladdin\\Jump_Spring.png", 60, PLAYER_SPRITE_HEIGHT, 8, 8, 5);

	State_Sprite[STATE_JUMP_ROPE] = new Sprite(L"Resource\\Sprite\\Aladdin\\Jump_Rope.png", 80, PLAYER_SPRITE_HEIGHT, 9, 9, 5);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}

bool Player::isClimbing()
{
	return m_isClimbing;
}

bool Player::isHanging()
{
	return m_isHanging;
}

bool Player::Update() 
{
	if (this->isHanging())
		{
		switch (m_state)
		{
		case STATE_THROW_HANG:
			if (m_sprite.is_action_frame())
			{
				if (!m_actacked)
				{
					m_actacked = true;
					Create_Apple();
				}
			}
			if (m_sprite.is_startFrame())
			{
				m_actacked = false;
			}
			break;
		}

			if (m_countdown_hang <= 0)
			{
				m_node.set_Accelerate(0, -GRAVITY.y);
				if (!Left_Right_Pressed)
				{
					m_node.set_Velocity(0, 0);
					Change_State(STATE_HANG);
				}
				else if (m_node.x() < Bar_left)
				{
					m_node.set_Velocity(0, 0);
					m_node.Translate(1, 0);
					Change_State(STATE_HANG);
				}
				else if (m_node.x() > Bar_right)
				{
					m_node.set_Velocity(0, 0);
					m_node.Translate(-1, 0);
					Change_State(STATE_HANG);
				}
			}

			Left_Right_Pressed = false;
			m_isHanging = false;
		}
	else if (this->isClimbing())
	{
		switch (m_state)
		{
		case STATE_THROW_HANG:
			if (m_sprite.is_action_frame())
			{
				if (!m_actacked)
				{
					m_actacked = true;
					Create_Apple();
				}
			}
			if (m_sprite.is_startFrame())
			{
				m_actacked = false;
			}
			break;
		}

		if (m_countdown <= 0)
		{
			m_node.set_Accelerate(0, -GRAVITY.y);
			if (!Up_Down_Pressed )
			{
				m_node.set_Velocity(0, 0);
				Change_State(STATE_CLIMB);
			}
			else if (m_node.y() <= Rope_max_height)
			{
				m_node.set_Velocity(0, 0);
				m_node.Translate(0, 1);
				Change_State(STATE_CLIMB);
			}
			
		}

		Up_Down_Pressed = false;
		m_isClimbing = false;
	}
	else if (this->is_Grounded())
	{
		if (m_node.get_Velocity().y > 0)
			m_node.set_Velocity(m_node.get_Velocity_X(), 0);

		switch (m_state)
		{
		case STATE_SLASH:
		case STATE_RUN_JUMP:
			Change_State(STATE_IDLE);
			break;
		case STATE_THROW:
		case STATE_THROW_DUCK:
		case STATE_THROW_JUMP:
			if (m_sprite.is_action_frame())
			{
				if (!m_actacked)
				{
					m_actacked = true;
					Create_Apple();
				}
			}
			if (m_sprite.is_startFrame())
			{
				m_actacked = false;
			}			
			break;
		}

		if (m_node.get_Velocity().x != 0)
			Change_State(STATE_RUN);
		else if(m_state == STATE_RUN)
			Change_State(STATE_IDLE);
	}
	else
	{
		switch (m_state)
		{

		case STATE_THROW_JUMP:
			if (m_sprite.is_action_frame())
			{
				if (!m_actacked)
				{
					m_actacked = true;
					Create_Apple();
				}
			}
			if (m_sprite.is_startFrame())
			{
				m_actacked = false;
			}
			break;

		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			m_node.set_Gravity(GRAVITY);
			m_node.set_Accelerate(m_node.get_Accelerate().x, 0);
			break;

		default:
			Change_State(STATE_RUN_JUMP);
			m_node.set_Gravity(GRAVITY);
			m_node.set_Accelerate(m_node.get_Accelerate().x, 0);
			break;
		}
	}

	m_node.Update();
	Update_Body_Size();
	m_rigid_body = Rect_Update();
	
	
	if (m_countdown > 0)
		m_countdown--;
	if (m_countdown_hang > 0)
		m_countdown_hang--;
	if (m_immute > 0)
		m_immute--;

		
	return true;
}

bool Player::Create_Apple()
{
	Sound::GetInstance()->Play("Object_Throw", false, 1);
	GameScene::getInstance()->CreateApple(m_node.get_Position(), VEC2(13 * m_facing, -2), VEC2(0, 0.5));
	m_apple--;
	return true;
}

bool Player::Update_Body_Size()
{
	switch (m_state)
	{
	case STATE_DUCK:
	case STATE_SLASH_DUCK:
	case STATE_THROW_DUCK:
		m_body_size = VEC2(PLAYER_WIDGHT, PLAYER_HEIGHT - 20);
		m_anchor_point = VEC2(0,30);
		return true;
	case STATE_CLIMB:
	case STATE_CLIMB_UP:
	case STATE_CLIMB_DOWN:
		m_body_size = VEC2(PLAYER_WIDGHT, PLAYER_HEIGHT + 20);
		m_anchor_point = VEC2(0, 0);
		return true;
	case STATE_HANG:
	case STATE_HANG_RUN:
	case STATE_SLASH_HANG:
	case STATE_THROW_HANG:
		m_body_size = VEC2(PLAYER_WIDGHT, PLAYER_HEIGHT + 20);
		m_anchor_point = VEC2(0,0);
		return true;
	default:
		m_body_size = VEC2(PLAYER_WIDGHT, PLAYER_HEIGHT);
		m_anchor_point = VEC2(0, 20);
		return true;
	}
}

bool Player::Jump(int type)
{	
	int __type = type;

	if (isHanging())
	{
		if (m_countdown_hang <= 0)
			{
			Change_State(__type);
			m_node.set_Velocity(m_node.get_Velocity_X(), 0);
			m_node.set_Accelerate(m_node.get_Accelerate_X(), 0);
			m_countdown_hang = HANG_COUNTDOWN;
			m_isHanging = false;
		}
	}
	else if (isClimbing())
		if (m_countdown <= 0)
		{
			{
				Change_State(STATE_JUMP_ROPE);
				m_node.set_Velocity(m_node.get_Velocity_X(), JUMP_HEIGHT);
				m_node.set_Accelerate(m_node.get_Accelerate_X(), 0);
				m_countdown = JUMP_OUT_ROPE_COUNTDOWN;
				m_isClimbing = false;
			}
		}

	if (is_Grounded())
	{	
		switch (m_state)
		{
		case STATE_IDLE:
		case STATE_RUN:	
			m_node.set_Velocity(m_node.get_Velocity_X(), JUMP_HEIGHT);
			Change_State(__type);
			return true;
		case STATE_RUN_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			Change_State(__type);
			m_node.set_Velocity(m_node.get_Velocity_X(), JUMP_HEIGHT);
			return true;
		default: 
			return false;
		}
	}
	else
	{
		switch (m_state)
		{
		case STATE_SLASH_JUMP:
			Change_State(__type);
			return true;

		default:
			return true;
		}
	}
	return false;
}

bool Player::Down()
{
	if (isClimbing())
	{
		Up_Down_Pressed = true;
		switch (m_state)
		{
		case STATE_SLASH_HANG:
		case STATE_THROW_HANG:
			Change_State(STATE_CLIMB_DOWN);
			return true;

		case STATE_CLIMB:
		case STATE_CLIMB_DOWN:
			set_Velocity(0, PLAYER_CLIMB_SPEED);
			Change_State(STATE_CLIMB_DOWN);
			return true;

		default:
			return false;
		}
	}
	if (is_Grounded())
	{	
		switch (m_state)
		{
		case STATE_IDLE:
		case STATE_SLASH_DUCK:
		case STATE_THROW_DUCK:
			Change_State(STATE_DUCK);
			return true;

		default:
			return false;
		}

		
		
	}
	return false;
}

bool Player::Move_Left()
{

	switch (m_state)
	{
	case STATE_RUN:
	case STATE_RUN_JUMP:
	case STATE_SLASH_JUMP:
	case STATE_THROW_JUMP:
	case STATE_JUMP_SPRING:
	case STATE_JUMP_ROPE:
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;


	case STATE_SLASH_DUCK:
	case STATE_THROW_DUCK:
		m_facing = FACE_LEFT;
		Change_State(STATE_DUCK);
		return true;


	case STATE_THROW:
	case STATE_SLASH:
		m_facing = FACE_LEFT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_STUNT:
		m_Blocking_Sprite = false;
		Change_State(STATE_RUN);
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_CLIMB:
		m_facing = FACE_LEFT;
		return true;

	case STATE_DUCK:
	case STATE_IDLE:
	case STATE_LOOKUP:
		Change_State(STATE_RUN);
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_HANG:
		Change_State(STATE_HANG_RUN);
		m_facing = FACE_LEFT;
		m_node.set_Velocity(-PLAYER_HANG_SPEED, 0);
		Left_Right_Pressed = true;
		return true;
	case STATE_HANG_RUN:
		m_facing = FACE_LEFT;
		m_node.set_Velocity(-PLAYER_HANG_SPEED, 0);
		Left_Right_Pressed = true;
		return true;
	}
	return false;
}

bool Player::Move_Right()
{

	switch (m_state)
	{
	case STATE_RUN:
	case STATE_RUN_JUMP:
	case STATE_SLASH_JUMP:
	case STATE_THROW_JUMP:
	case STATE_JUMP_SPRING:
	case STATE_JUMP_ROPE:
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_SLASH_DUCK:
	case STATE_THROW_DUCK:
		m_facing = FACE_RIGHT;
		Change_State(STATE_DUCK);
		return true;

	case STATE_THROW:
	case STATE_SLASH:
		m_facing = FACE_RIGHT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_STUNT:
		m_Blocking_Sprite = false;
		Change_State(STATE_RUN);
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_CLIMB:
		m_facing = FACE_RIGHT;
		return true;

	case STATE_LOOKUP:
	case STATE_DUCK:
	case STATE_IDLE:
		Change_State(STATE_RUN);
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_HANG:
		Change_State(STATE_HANG_RUN);
		m_facing = FACE_RIGHT;
		m_node.set_Velocity(PLAYER_HANG_SPEED, 0);
		Left_Right_Pressed = true;
		return true;
	case STATE_HANG_RUN:
		m_facing = FACE_RIGHT;
		m_node.set_Velocity(PLAYER_HANG_SPEED, 0);
		Left_Right_Pressed = true;
	}
	return false;
}

bool Player::Stand()
{
	if(is_Grounded())
		switch (m_state)
		{	
		case STATE_RUN:
		case STATE_RUN_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			m_node.set_Accelerate(0, 0);
			Change_State(STATE_IDLE);
			return true;
		case STATE_STUNT:
		case STATE_LOOKUP:
		case STATE_DUCK:
		case STATE_THROW:
		case STATE_SLASH:
			Change_State(STATE_IDLE);
			return true;
		case STATE_THROW_JUMP:
		case STATE_SLASH_JUMP:
			m_Blocking_Sprite = false;
			Change_State(STATE_IDLE);
			return true;
		case STATE_SLASH_DUCK:
		case STATE_THROW_DUCK:
			Change_State(STATE_DUCK);
			return true;
		default:
			return false;
		}	
	else
	{
		switch (m_state)
		{
		case STATE_RUN_JUMP:
		case STATE_JUMP_SPRING:
			m_node.set_Accelerate(0, 0);
			return true;
		}
	}
}

bool Player::Slash()
{
	if (isHanging() || isClimbing())
	{
		switch (m_state)
		{
		case STATE_CLIMB:
		case STATE_HANG:
			Change_State(STATE_SLASH_HANG, true);
			//Sound::GetInstance()->Play("High_Sword", false, 1);
			return true;
		}
	}
	else if (is_Grounded())
	{
		switch (m_state)
		{
		case STATE_IDLE:
			Change_State(STATE_SLASH, true);
			//Sound::GetInstance()->Play("High_Sword", false, 1);
			return true;
		case STATE_RUN:
			m_node.set_Velocity(0, 0);
			m_node.set_Accelerate(0, 0);
			Change_State(STATE_SLASH, true);
			//Sound::GetInstance()->Play("High_Sword", false, 1);
			return true;
		case STATE_DUCK:
			Change_State(STATE_SLASH_DUCK, true);
			//Sound::GetInstance()->Play("High_Sword", false, 1);
			return true;

		default:
			return false;
		}
		
	}
	else
	{
		switch (m_state)
		{
		case STATE_RUN_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			Change_State(STATE_SLASH_JUMP, true);
			//Sound::GetInstance()->Play("High_Sword", false, 1);
			return true;

		default:
			return false;
		}

	}
	return false;
}

bool Player::Throw()
{
	if (isHanging())
	{
		switch (m_state)
		{
		case STATE_HANG_RUN:
		case STATE_HANG:
			m_node.set_Velocity(0, 0);
			Change_State(STATE_THROW_HANG, true);
			return true;
		}
	}
	else if (isClimbing())
	{
		switch (m_state)
		{
		case STATE_CLIMB:
		case STATE_CLIMB_DOWN:
		case STATE_CLIMB_UP:
			m_node.set_Velocity(0, 0);
			Change_State(STATE_THROW_HANG, true);
			return true;
		}
	}
	else if (is_Grounded())
		switch (m_state)
			{
			case STATE_IDLE:
			case STATE_RUN :
				m_node.set_Velocity(0, 0);
				m_node.set_Accelerate(0, 0);
				Change_State(STATE_THROW, true);
				return true;
			case STATE_DUCK:
				Change_State(STATE_THROW_DUCK, true);
				return true;
			default:
				return false;
			}
	else
	{
		switch (m_state)
		{
		case STATE_RUN_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			Change_State(STATE_THROW_JUMP, true);
			return true;

		default:
			return false;
		}
	}

	return false;
}

bool Player::Up()
{
	if (isClimbing())
	{
		Up_Down_Pressed = true;
		switch (m_state)
		{
		case STATE_SLASH_HANG:
		case STATE_THROW_HANG:
			Change_State(STATE_CLIMB_UP);
			return true;

		case STATE_CLIMB:
		case STATE_CLIMB_UP:
			set_Velocity(0, -PLAYER_CLIMB_SPEED);
			Change_State(STATE_CLIMB_UP);
			return true;
		default:
			return false;
		}
	}
	else if(is_Grounded())
		switch (m_state)
		{
		case STATE_IDLE:
			Change_State(STATE_LOOKUP);
			return true;
		case STATE_LOOKUP:
			Camera::getInstance()->Lookup();
			return true;
		default:
			return false;
		}
}

bool Player::Climb(Rope* rope)
{
	Rope_max_height = rope->get_Rect().top;
	if (m_countdown <= 0 )
	{
		m_isClimbing = true;
		m_isGrounded = false;
		Up_Down_Pressed = false;
		switch (m_state)
		{
		case STATE_RUN_JUMP:
		case STATE_SLASH_JUMP:
		case STATE_THROW_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			m_Blocking_Sprite = false;
			Change_State(STATE_CLIMB);
			return true;

		default:
			return false;
		}
	}
	else
	{
		m_countdown = JUMP_OUT_ROPE_COUNTDOWN;
	}
}

bool Player::Hang(HorizontalBar* bar)
{
	Bar_left = bar->get_Rect().left;
	Bar_right = bar->get_Rect().right;
	if (m_countdown_hang <= 0)
	{
		m_isHanging = true;
		m_isClimbing = false;
		m_isGrounded = false;
		m_countdown = 0;
		Left_Right_Pressed = false;
		switch (m_state)
		{
		case STATE_RUN_JUMP:
		case STATE_SLASH_JUMP:
		case STATE_THROW_JUMP:
		case STATE_JUMP_SPRING:
		case STATE_JUMP_ROPE:
			m_Blocking_Sprite = false;
			Change_State(STATE_HANG);
			return true;

		default:
			return false;
		}
	}
	else
	{
		m_countdown_hang = HANG_COUNTDOWN;
	}
}

bool Player::Add_apple()
{
	m_apple++;
	return true;
}

int Player::Get_apple_amount()
{
	return m_apple;
}

bool Player::Add_ruby()
{
	m_ruby++;
	return true;
}

int Player::Get_ruby_amount()
{
	return m_ruby;
}

int Player::is_onCountdown_Climb()
{
	if (m_countdown > 0)
	{
		m_countdown = JUMP_OUT_ROPE_COUNTDOWN;
	}
	return m_countdown >0;
}

int Player::is_onCountdown_Hang()
{
	if (m_countdown_hang > 0)
	{
		m_countdown_hang = HANG_COUNTDOWN;
	}
	return m_countdown_hang >0;
}

int Player::Hit()
{
	if (m_immute == 0)
	{
		if (is_Grounded())
		{	
			m_Blocking_Sprite = false;
			Change_State(STATE_STUNT, true);
			m_node.set_Accelerate(0, 0);
		}
		m_immute = 60;
		Sound::GetInstance()->Play("Aladdin_Hurt");
		m_health--;
	}
	if (m_health == 0)
	{
		m_live--;
		if (m_live >= 0)
		{
			m_health = 8;
			GameScene::getInstance()->Respawn();
		}
		else
		{
			Reset_Stat();
			GameScene::getInstance()->New();
		}

	}
		


	return m_health;
}

bool  Player::Reset_Stat()
{
	m_health = 8;
	m_score = 0;
	m_ruby = 0;
	m_apple = 8;
	m_live = 3;
	return true;
}

int Player::get_Health()
{
	return m_health;
}

int Player::Extra_Health()
{
	m_health += 3;
	if (m_health > 8)
		m_health = 8;
	return m_health;
}

int Player::Set_Apple_Amount(int amount)
{
	m_apple = amount;
	return m_apple;
}

int Player::Set_Healh(int amount)
{
	m_health = amount;
	return m_health;
}

bool Player::Add_score(int score)
{
	m_score += score;
	return  true;
}

bool Player::Add_lives()
{
	return m_live++;
}

int Player::Get_score()
{
	return m_score;
}

int Player::Get_live()
{
	return m_live;
}


