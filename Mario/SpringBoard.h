#pragma once
#include "Object.h"
class SpringBoard :
	public Object
{
public:
	SpringBoard(int X, int Y);
	~SpringBoard();
	bool Update() override;
	int Hit() override;
private:
	bool Init_Sprite();
};

