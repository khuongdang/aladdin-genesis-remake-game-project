#pragma once
#include "Object.h"
#include <algorithm>
#include "Camera.h"

using namespace std;


class QuadTree
{
public:
	QuadTree(int X = 1);
	~QuadTree();

	bool Create_Child(int X);
	Object* Add(Object* object);
	bool Update();
	bool Render();

	bool isNode();

	QuadTree* Left_top = nullptr;
	QuadTree* Left_bot = nullptr;
	QuadTree* Right_top = nullptr;
	QuadTree* Right_bot = nullptr;

	vector<Object*> m_object = vector<Object*>(0);

	RECT Set_bound(RECT rect);

	vector<Object*> getObject();

	vector<int> m_destroy_list = vector<int>(0);
	bool Destroy(int i);

	bool Destroy(Object * object);

private:

	bool m_isNode;
	Camera* m_camera = Camera::getInstance();
	RECT m_bound = RECT();


};

