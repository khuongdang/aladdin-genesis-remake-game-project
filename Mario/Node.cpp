#include "Node.h"

Node::Node(VEC2 Position, VEC2 Gravity, VEC2 Velocity, VEC2 Accelerate, float maxspeed, float friction,float maxfalling )
{
	m_Position = Position;
	m_Gravity = Gravity;
	m_Accelerate = Accelerate;
	m_Velocity = Velocity;

	m_maxspeed = maxspeed;
	m_friction = friction;
	m_maxfallingSpeed = maxfalling;
}

Node::~Node()
{
}

VEC2 Node::set_Position(VEC2 Position)
{
	m_Position = Position;

	return m_Position;
}

VEC2 Node::set_Position(float x, float y)
{
	m_Position = VEC2(x, y);

	return m_Position;
}

VEC2 Node::Translate(VEC2 vec)
{
	m_Position += vec;
	
	return m_Position;
}

VEC2 Node::Translate(float x, float y)
{
	m_Position += VEC2(x,y);

	return m_Position;
}

VEC2 Node::set_Gravity(VEC2 Gravity)
{
	m_Gravity = Gravity;

	return m_Gravity;
}

VEC2 Node::set_Gravity(float x, float y)
{
	m_Gravity = VEC2(x, y);

	return m_Gravity;
}

VEC2 Node::set_Velocity(VEC2 Velocity)
{
	m_Velocity = Velocity;

	return m_Velocity;
}

VEC2 Node::set_Velocity(float x, float y)
{
	m_Velocity= VEC2(x, y);

	return m_Velocity;
}

VEC2 Node::set_Accelerate(VEC2 Accelerate)
{
	m_Accelerate = Accelerate;

	return m_Accelerate;
}

VEC2 Node::set_Accelerate(float x, float y)
{
	m_Accelerate = VEC2(x, y);

	return m_Accelerate;
}

VEC2 Node::get_Position()
{
	return m_Position;
}

float Node::x()
{
	return m_Position.x;
}

float Node::y()
{
	return m_Position.y;
}

VEC2 Node::get_Gravity()
{
	return m_Gravity;
}

float Node::get_Gravity_X()
{
	return m_Gravity.x;
}

float Node::get_Gravity_Y()
{
	return m_Gravity.y;
}

VEC2 Node::get_Velocity()
{
	return m_Velocity;
}

float Node::get_Velocity_X()
{
	return m_Velocity.x;
}

float Node::get_Velocity_Y()
{
	return m_Velocity.y;
}

VEC2 Node::get_Accelerate()
{
	return m_Accelerate;
}

float Node::get_Accelerate_X()
{
	return m_Accelerate.x;
}

float Node::get_Accelerate_Y()
{
	return m_Accelerate.y;
}

float Node::Set_maxSpeed(float maxspeed)
{
	m_maxspeed = maxspeed;
	return m_maxspeed;
}

float Node::set_maxFallingSpeed(float param)
{
	m_maxfallingSpeed = param;
	return m_maxfallingSpeed;
}

float Node::get_maxFallingSpeed()
{
	return m_maxfallingSpeed;
}

float Node::Get_maxSpeed()
{
	return m_maxspeed;
}

float Node::Set_friction(float friction)
{
	m_friction = friction;
	return m_friction;
}

float Node::Get_friction()
{
	return m_friction;
}

bool Node::Update()
{
	if (Math::abs(m_Velocity.x) > m_friction)
	{
		if (m_Velocity.x < 0.0)
			m_Velocity.x += m_friction;
		else
			m_Velocity.x -= m_friction;
	}
	else
		m_Velocity.x = 0.0;

	m_Velocity += (m_Accelerate + m_Gravity);
	m_Position += m_Velocity;

	if (m_maxspeed != 0)
	{
		if (m_Velocity.x > m_maxspeed)
			m_Velocity.x = m_maxspeed;
		else if (m_Velocity.x < -m_maxspeed)
			m_Velocity.x = -m_maxspeed;
	}

	if (m_maxfallingSpeed != 0)
	{
		if (m_Velocity.y > m_maxfallingSpeed)
			m_Velocity.y = m_maxfallingSpeed;
	}
	
	return false;
}

