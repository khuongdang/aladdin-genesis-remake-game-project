#pragma once
#include "Projectile.h"
#include "QuadTree.h"

class Spell :
	public Projectile
{
public:
	Spell(float x, float y);
	~Spell();
	bool Update();
	bool Init_Sprite();
	int Hit();
};

