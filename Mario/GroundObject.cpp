#include "GroundObject.h"



GroundObject::GroundObject()
{
}


GroundObject::~GroundObject()
{
}

bool GroundObject::is_Grounded()
{
	return m_isGrounded;
}

bool GroundObject::set_Grounded(bool param)
{
	m_isGrounded = param;
	return m_isGrounded;
}

