#pragma once

#include "System.h"

#include "GroundObject.h"
#include "Rope.h"
#include "HorizontalBar.h"

class Player : public GroundObject
{
public:
	Player(int X = 100, int Y= 100);
	~Player();
	
	bool Update() override;

	bool Create_Apple();

	bool Jump(int type = STATE_RUN_JUMP);
	bool Down() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Stand() override;
	bool Slash();
	bool Throw();
	bool Up();
	bool Climb(Rope* rope);
	bool Hang(HorizontalBar* bar);


	int Get_ruby_amount();

	bool isHanging();

	int is_onCountdown_Climb();

	int is_onCountdown_Hang();

	int Hit();

	bool Reset_Stat();

	int Extra_Health();

	int Set_Apple_Amount(int amount);

	int Set_Healh(int amount);

	bool Add_apple();
	bool Add_ruby();
	bool Add_score(int score); // them diem
	bool Add_lives();

	int Get_apple_amount();
	int Get_score();
	int get_Health();
	int Get_live();



private:
	bool Up_Down_Pressed = false;
	float Rope_max_height = 0;
	bool m_isClimbing = false;
	bool isClimbing();

	float Bar_left=0;
	float Bar_right = 0;
	bool m_isHanging = false;
	bool Left_Right_Pressed = false;

	int m_countdown_hang = 0;
	int m_countdown = 0;
	int immute_countdown = 0;

	int m_apple = 0;
	int m_health = 8;
	int m_ruby = 0;
	int m_score = 0;
	int m_live = 3;

	bool Update_Body_Size();
	bool Init_Sprite() override;



	
};

