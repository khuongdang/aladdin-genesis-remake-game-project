#pragma once
#include "GroundEnemy.h"
#include "Player.h"

class Jafar : public GroundEnemy
{
public:
	Jafar(int X, int Y, Player* Following);
	~Jafar();
	bool Update();
	bool Throw();
	bool Move_Left();
	bool Move_Right();
	bool Init_Sprite();
	bool Init_Snake();
	int Hit() override;
	int m_countdown;

	int m__state;
};

