#include "KnifeGuy.h"
#include "GameScene.h"


KnifeGuy::KnifeGuy(int X, int Y, Player* Following)
{
	m_TypeName = L"ENEMY";
	m_Following = Following;

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_node.Set_maxSpeed(0);
	m_node.Set_friction(0);
	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_facing = FACE_LEFT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(40, 60);
	m_anchor_point = VEC2(0, 16);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_heath = 1;
	m_isGrounded = false;
	m_isStatic = false;
}


KnifeGuy::~KnifeGuy()
{
}

bool KnifeGuy::Update()
{
	if (m_Following != nullptr)
	{
		VEC2 player = m_Following->get_Position();
		VEC2 pos = m_node.get_Position();

		float distance = player.x - pos.x;
		float y = player.y - pos.y;

		if (Math::abs(distance) <= 150 && abs(y)<50)
		{
			if (distance > 0)
				Move_Right();
			else
				Move_Left();
			Slash();
		}
		else if (Math::abs(distance) <= 300 && abs(y)<50)
		{
			if (distance > 0)
				Move_Right();
			else
				Move_Left();
			Throw();
		}

	}


	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool KnifeGuy::Move_Left()
{
	m_facing = FACE_LEFT;
	return true;
}

bool KnifeGuy::Move_Right()
{
	m_facing = FACE_RIGHT;
	return true;
}

bool KnifeGuy::Throw()
{
	if (m_sprite.is_endFrame())
		{
			if (!attacked)
			{
				float dis = m_Following->get_Position().x - m_node.get_Position().x;

				float vel = dis / (15 * 2 / GRAVITY.y);

				GameScene::getInstance()->CreateKnife(m_node.get_Position(), VEC2( vel  , -15),GRAVITY);
				attacked = true;
				return true;
			};
		}
		else
			attacked = false;
	return false;
}

bool KnifeGuy::Slash()
{
	if (m_sprite.is_endFrame())
	{
		if (!attacked)
		{
			float dis = m_Following->get_Position().x - m_node.get_Position().x;

			float vel = dis / (5 * 2 / GRAVITY.y);

			GameScene::getInstance()->CreateKnife(m_node.get_Position(), VEC2(vel, -5), GRAVITY);
			attacked = true;
			return true;
		};
	}
	else
		attacked = false;
	return false;
}

bool KnifeGuy::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\KnifeGuy\\Idle.png", 80, 100, 11, 11, 5);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();
	return true;
}
