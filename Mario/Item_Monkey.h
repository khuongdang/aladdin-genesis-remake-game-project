#pragma once
#include "Object.h"
class Item_Monkey :
	public Object
{
public:
	Item_Monkey(VEC2 pos);
	~Item_Monkey();

	bool Init_Sprite();
	int Hit();
};

