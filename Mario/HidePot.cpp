#include "HidePot.h"
#include "GameScene.h"


HidePot::HidePot(int X, int Y, Player* Following)
{
	m_TypeName = L"ENEMY";
	m_Following = Following;

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos, GRAVITY);

	Init_Sprite();

	m_node.Set_maxSpeed(PLAYER_MAX_SPEED - 3);
	m_node.Set_friction(PLAYER_FRICTION);
	m_node.set_maxFallingSpeed(PLAYER_MAX_FALLING_SPEED);

	m_facing = FACE_LEFT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(50, 50);
	//m_anchor_point = VEC2(0, 0);				// ( 100(sprite height) - 60(Player height) )/2
	m_rigid_body = Rect_Update();

	m_heath = 1;
	m_isGrounded = false;
	m_isStatic = false;
}


HidePot::~HidePot()
{
}

bool HidePot::Update()
{
	if (m_Following != nullptr)
	{
		VEC2 player = m_Following->get_Position();
		VEC2 pos = m_node.get_Position();

		float distance = player.x - pos.x;
		float y = player.y - pos.y;

		if (Math::abs(distance) <= 60 && abs(y)<50)
		{
			if (distance > 0)
				Move_Right();
			else
				Move_Left();
			Slash();
		}
		else if (Math::abs(distance) <= 250 && abs(y)<50)
		{
			if (distance > 0)
				Move_Right();
			else
				Move_Left();
		}
		else
		{
			Stand();
		}
	}

	if (m_state == STATE_SLASH)
	{
		if (m_sprite.is_action_frame())
		{
			if (!attacked)
			{
				VEC2 pos = m_node.get_Position();
				pos.x += 60 * m_facing;
				GameScene::getInstance()->Enemy_Slash(pos);
			}
		}
		else if (m_sprite.is_startFrame())
			attacked = false;
	}

	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool HidePot::Move_Left()
{
	switch (m_state)
	{
	case STATE_RUN:
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_SLASH:
		m_facing = FACE_LEFT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_IDLE:
		Change_State(STATE_RUN);
		m_facing = FACE_LEFT;
		m_node.set_Accelerate(-PLAYER_MOVE_ACCELERATE, 0);
		return true;
	}
	return false;
}

bool HidePot::Move_Right()
{
	switch (m_state)
	{
	case STATE_RUN:
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;

	case STATE_SLASH:
		m_facing = FACE_RIGHT;
		Change_State(STATE_IDLE);
		return true;

	case STATE_IDLE:
		Change_State(STATE_RUN);
		m_facing = FACE_RIGHT;
		m_node.set_Accelerate(PLAYER_MOVE_ACCELERATE, 0);
		return true;
	}
	return false;
}

bool HidePot::Slash()
{
	if (is_Grounded())
	{
		switch (m_state)
		{
		case STATE_IDLE:
			Change_State(STATE_SLASH, true);
			return true;
		case STATE_RUN:
			m_node.set_Velocity(0, 0);
			m_node.set_Accelerate(0, 0);
			Change_State(STATE_SLASH, true);
			return true;
		default:
			return false;
		}
	}

	return false;
}

bool HidePot::Stand()
{
	if (is_Grounded())
		switch (m_state)
		{
		case STATE_RUN:
			m_node.set_Accelerate(0, 0);
			Change_State(STATE_IDLE);
			return true;
		case STATE_SLASH:
			Change_State(STATE_IDLE);
			return true;
		default:
			return false;
		}
}

bool HidePot::Init_Sprite()
{
	State_Sprite.resize(10);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\HidePot\\Idle.png", 50, 70, 6, 6,7);

	State_Sprite[STATE_RUN] = new Sprite(L"Resource\\Sprite\\Enemy\\HidePot\\Run.png", 60, 70, 10, 5, 5);

	State_Sprite[STATE_SLASH] = new Sprite(L"Resource\\Sprite\\Enemy\\HidePot\\Slash.png", 90, 70, 4, 4, 7,0,3);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();
	return true;
}
