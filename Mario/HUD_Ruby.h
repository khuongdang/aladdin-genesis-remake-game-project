#pragma once
#include "System.h"
#include "HUD_Number.h"

class HUD_Ruby
{
public:
	HUD_Ruby();
	~HUD_Ruby();

	bool Render(VEC2 vec, int amount);

	bool Render(float x, float y, int amount);

	Sprite m_sprite = Sprite();
	HUD_Number* display;
	bool Init_Sprite();
};

