#pragma once
#include "Object.h"
class Item_Health :
	public Object
{
public:
	Item_Health(VEC2 pos);
	~Item_Health();

	bool Init_Sprite();
	int Hit();
};

