#pragma once
#include "GameScene.h"

class FXmanager
{
public:
	FXmanager(GameScene* game);
	~FXmanager();

	Object* CreateFX(LPWSTR name, VEC2 pos);

	static FXmanager* FXmanager::get_instance();

private:
	static FXmanager* m_instance;
};

