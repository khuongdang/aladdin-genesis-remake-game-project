#include "Knife.h"
#include "Sound.h"



Knife::Knife(float x, float y)
{
	m_TypeName = L"KNIFE";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(20, 20);	//temp
	m_rigid_body = Rect_Update();
	Sound::GetInstance()->Play("Object_Throw");
}

bool Knife::Update()
{
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

bool Knife::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Projectile\\Knife.png", 40, 40, 7, 7,5);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}

int Knife::Hit()
{
	return m_parent->Destroy(this);
}



Knife::~Knife()
{
}
