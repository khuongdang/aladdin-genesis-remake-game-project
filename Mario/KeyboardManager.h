#pragma once

#include "System.h"
#include "GameScene.h"


class KeyboardManager
{
public:
	KeyboardManager(HINSTANCE* hInstance, HWND* hWnd);
	~KeyboardManager();
	bool Init();
	bool ProcessKeyboard();
	bool OnkeyHold();
	bool IsKeyDown(int KeyCode);
	bool OnKeyUp(int KeyCode);
	bool OnKeyDown(int KeyCode);
	bool AcquireKeyboard();

	static KeyboardManager* getInstance();


private:
	KeyboardManager();
	static KeyboardManager m_Instance;

	HINSTANCE* m_hInstance;
	HWND* m_hwnd;

	LPDIRECTINPUT8       m_di;		// The DirectInput object         
	LPDIRECTINPUTDEVICE8 m_didev;	// The keyboard device 

	BYTE  m_KeyStates[256];			// DirectInput keyboard state buffer 

									// Buffered keyboard data
	DIDEVICEOBJECTDATA m_KeyEvents[KEYBOARD_BUFFER_SIZE];
};

