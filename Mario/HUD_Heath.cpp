#include "HUD_Heath.h"



HUD_Heath::HUD_Heath()
{
	Init_Sprite();
	m_heath = 0;
}


HUD_Heath::~HUD_Heath()
{
}

bool HUD_Heath::Render(VEC2 vec, int health)
{
	if (health >= 0 && health <= 8)
	{
		if (health != m_heath)
		{
			m_heath = health;
			m_sprite = *m_State_Sprite[health];
		}

		if (m_sprite.isInitailized())
		{
			vec.x -= (61 - m_sprite.get_width() / 2);
			m_sprite.Render(vec, 1);
			m_sprite.Update();
		}
	}

	return true;
}


bool HUD_Heath::Render(float x, float y,int health)
{
	return Render(VEC2(x, y), health);
}


bool HUD_Heath::Init_Sprite()
{
	m_State_Sprite[0] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\0.png", 50, 26, 1, 1);

	m_State_Sprite[1] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\1.png", 66, 32, 4, 4,5);

	m_State_Sprite[2] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\2.png", 74, 34, 4, 4, 5);

	m_State_Sprite[3] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\3.png", 82, 34, 4, 4, 5);

	m_State_Sprite[4] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\4.png", 90, 34, 4, 4, 5);

	m_State_Sprite[5] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\5.png", 98, 34, 4, 4, 5);

	m_State_Sprite[6] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\6.png", 106, 34, 4, 4, 5);

	m_State_Sprite[7] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\7.png", 114, 34, 4, 4, 5);

	m_State_Sprite[8] = new Sprite(L"Resource\\Sprite\\HUD\\Health\\8.png", 122, 34, 4, 4, 5);

	m_sprite = *m_State_Sprite[0];

	m_State_Sprite.shrink_to_fit();

	return true;
}
