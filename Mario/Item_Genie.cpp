#include "Item_Genie.h"
#include "Sound.h"
#include "GameScene.h"


Item_Genie::Item_Genie(VEC2 pos)
{
	m_TypeName = L"ITEM_GENIE";

	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(45, 50);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Item_Genie::~Item_Genie()
{
}

bool Item_Genie::Init_Sprite()
{
	State_Sprite.resize(2);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Item\\Genie\\Genie.png", 60, 60, 4, 4, 7);
	State_Sprite[STATE_STUNT] = new Sprite(L"Resource\\Sprite\\Item\\Genie\\Genie_explosion.png", 88, 88, 15,15,5);
	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}

bool Item_Genie::Update()
{
	if (m_state == STATE_STUNT)
	{
		if (m_sprite.is_endFrame())
		{
			if (m_parent != nullptr) m_parent->Destroy(this);
		}
	}
	return true;
}


int Item_Genie::Hit()
{
	Change_State(STATE_STUNT);
	
		Sound::GetInstance()->Play("Gem Collect");
		GameScene::getInstance()->Add_Score(300);
	return 0;
}
