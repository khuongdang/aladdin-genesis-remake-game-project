#pragma once
#include "System.h"
#include "Object.h"

class Trigger : public Object
{
public:
	Trigger(float x, float y, float width, float height);
	~Trigger();

	bool Update();

	int countdown = 0;
	LPWSTR m_SubType;
	int m_SubIndex;
	bool State = false;
	
};

