#pragma once
#include "System.h"
#include "Node.h"

class Sprite
{
public:
	Sprite();
	Sprite(	LPWSTR Path,
			int Width, 
			int Height, 
			int Count, 
			int SpritePerRow,
			int FrameCount = 20,
			int StartFrame = 0,
			int ActionFrame = 0			
	);
	~Sprite();

	int get_Index();
	int get_Count();

	bool Update();
	bool Render(VEC2 vec, int state);
	bool Render(float X, float Y, int state);

	int get_height();
	int get_width();

	bool isdone();
	bool is_endFrame();
	bool is_startFrame();

	bool is_action_frame();

	bool isInitailized();

protected:
	LPDIRECT3DDEVICE9 m_d3ddv;

	LPDIRECT3DTEXTURE9 m_image;
	LPD3DXSPRITE* m_spriteHandler;

	int m_index;
	int m_width;
	int m_height;
	int m_count;
	int m_spritePerRow;
	int m_frameCount;
	int m_start_frame;
	int m_action_frame;

	int FrameCurrent = 0;

	bool m_isdone;

	VEC3 m_position;
	VEC3 m_center;
	VEC3 m_scale;

	D3DXMATRIX m_tranform;

	int m_facing;

	bool m_inited = false;
};

