#pragma once
#include "Object.h"
class Coal :
	public Object
{
public:
	Coal(float x, float y, float width, float height);
	~Coal();
	bool Update() override;
	bool Create_Fire(VEC2 pos);

	int countdown = 0;
};

