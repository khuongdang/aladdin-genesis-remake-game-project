#include "HUD_Apple.h"



HUD_Apple::HUD_Apple()
{
	Init_Sprite();
	display = new HUD_Number();
}


HUD_Apple::~HUD_Apple()
{
}

bool HUD_Apple::Render(VEC2 vec, int amount)
{
	if (amount > 0)
	{
		vector<int> split = vector<int>(0);
		
		int temp = amount;
		while (temp > 0)
		{
			split.push_back(temp % 10);
			temp /= 10;
		}

		int size = split.size();
		for (int i = 0; i < size; i++)
		{
			VEC2 pos = vec;
			pos.x += 18 * (size - i);
			display->Render(pos, split[i]);
		}

		
	}
	else
	{
		VEC2 pos = vec;
		pos.x += 18;
		display->Render(pos, 0);
	}

	m_sprite.Render(vec, 1);

	return true;
}

bool HUD_Apple::Render(float x, float y, int amount)
{
	return Render(VEC2(x, y), amount);
}

bool HUD_Apple::Init_Sprite()
{
	m_sprite = Sprite(L"Resource\\Sprite\\HUD\\Apple\\Icon.png", 18, 18, 1, 1);
	return true;
}
