#pragma once
#include "Object.h"

class Wall : public Object
{
public:

	Wall(float x, float y, float width, float height);
	~Wall();
};

