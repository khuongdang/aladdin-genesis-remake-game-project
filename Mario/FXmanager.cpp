#include "FXmanager.h"
#include "FX.h"


FXmanager::FXmanager(GameScene* game)
{
	if (m_instance == nullptr)
		m_instance = this;
}

FXmanager::~FXmanager()
{
}

Object * FXmanager::CreateFX(LPWSTR name, VEC2 pos)
{
	Object* a = nullptr;
	if (name == L"Explode_On_Dead")
		a = new FX_Explode_On_Dead(pos);
	else if (name == L"Pot_Explode")
		a = new FX_Pot_Explode(pos);
	else if (name == L"Fire_Coal")
		a = new FX_Fire_Coal(pos);
	else if (name == L"Fire_Coal")
		a = new FX_Fire_Coal(pos);
	else if (name == L"Apple_Explode")
		a = new FX_Apple_Explode(pos);

	if(a != nullptr)
		if (GameScene::getInstance() != nullptr)
		{
			GameScene::getInstance()->CreateFX(a);
			return a;
		}
			
	return nullptr;
}

FXmanager * FXmanager::get_instance()
{
	return m_instance;
}

FXmanager* FXmanager::m_instance = nullptr;
