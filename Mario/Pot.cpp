#include "Pot.h"
#include "FXmanager.h"


Pot::Pot(float x, float y)
{
	m_TypeName = L"KNIFE";

	VEC2 pos = VEC2(x, y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(30, 30);
	m_rigid_body = Rect_Update();
}

bool Pot::Update()
{
	m_node.Update();
	m_rigid_body = Rect_Update();
	return true;
}

int Pot::Hit()
{
	FXmanager::get_instance()->CreateFX(L"Pot_Explode", m_node.get_Position());
	return m_parent->Destroy(this);
}


Pot::~Pot()
{
}

bool Pot::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Enemy\\Projectile\\Pot.png", 40, 40, 6, 6, 2);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
