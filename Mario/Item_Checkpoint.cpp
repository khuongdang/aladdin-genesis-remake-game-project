#include "Item_Checkpoint.h"
#include "Sound.h"

Item_Checkpoint::Item_Checkpoint(VEC2 pos)
{
	m_TypeName = L"ITEM_CHECKPOINT";
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(25, 36);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Item_Checkpoint::~Item_Checkpoint()
{
}

bool Item_Checkpoint::Update()
{
	if (m_state == 1)
	{
		if (m_sprite.is_endFrame())
		{
			Change_State(2);
		}
	}
	return true;
}

bool Item_Checkpoint::Init_Sprite()
{
	State_Sprite.resize(3);

	State_Sprite[0] = new Sprite(L"Resource\\Sprite\\Item\\Checkpoint\\Idle.png", 40, 40, 1, 1);

	State_Sprite[1] = new Sprite(L"Resource\\Sprite\\Item\\Checkpoint\\checkpoint.png", 40, 40, 10, 10, 5);

	State_Sprite[2] = new Sprite(L"Resource\\Sprite\\Item\\Checkpoint\\Activated.png", 40, 40, 1, 1);

	m_sprite = *State_Sprite[0];

	State_Sprite.shrink_to_fit();

	return true;
}

int Item_Checkpoint::Hit()
{
	if (!is_activated)
	{
		Sound::GetInstance()->Play("Continue Point");
		is_activated = true;
		Change_State(1);
	}
	return 0;
}
