#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include "FrameWork.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	FrameWork* framework = new FrameWork(&hInstance, nCmdShow);
	framework->Init();
	framework->Run();
	framework->End();

	//GameEnd();

	return 0;
}