#pragma once
#include "Object.h"
#include "QuadTree.h"

class FX_Explode_On_Dead :
	public Object
{
public:
	FX_Explode_On_Dead(VEC2 pos);
	~FX_Explode_On_Dead();
	bool Update();
	bool Init_Sprite();
};

