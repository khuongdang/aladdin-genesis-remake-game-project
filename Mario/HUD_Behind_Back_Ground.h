#pragma once
#include "System.h"
#include "Sprite.h"
#include "Camera.h"

using namespace std;

class HUD_Behind_Back_Ground
{
public:
	HUD_Behind_Back_Ground();
	~HUD_Behind_Back_Ground();

	bool Render();

	Sprite* add_sprite(Sprite* sprite);
	Camera* set_cam(Camera* cam);

private:
	bool Init_Sprite();
	vector<Sprite*> m_State_Sprite = vector<Sprite*>(0);

	LPWSTR m_Path;
	Camera* m_camera;
	int m_Number_Of_Sprite;
};

