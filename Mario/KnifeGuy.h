#pragma once
#include "GroundEnemy.h"

class KnifeGuy :
	public GroundEnemy
{
public:
	KnifeGuy(int X, int Y, Player* Following = nullptr);
	~KnifeGuy();

	bool Update() override;
	bool Move_Left() override;
	bool Move_Right() override;
	bool Slash();
	bool Throw();

private:

	bool Init_Sprite() override;
};

