#pragma once
#include "System.h"
#include "Sprite.h"

using namespace std;
class HUD_Desert_Number
{
public:
	HUD_Desert_Number();
	~HUD_Desert_Number();
	bool Render(VEC2 vec, int index);

	bool Render(float x, float y, int index);
private:
	Sprite m_sprite = Sprite();
	vector<Sprite*> m_State_Sprite = vector<Sprite*>(10);
	bool Init_Sprite();
	VEC2 m_Position;
	int m_index;
};

