#include "FX_Fire_Coal.h"
#include "Sound.h"


FX_Fire_Coal::FX_Fire_Coal(VEC2 pos)
{
	m_TypeName = L"FX";

	m_node = Node(pos);

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(30, 50);

	m_rigid_body = Rect_Update();
	Init_Sprite();

	Sound::GetInstance()->Play("Fire From Coal");
}


FX_Fire_Coal::~FX_Fire_Coal()
{
}

bool FX_Fire_Coal::Update()
{
	if (m_sprite.is_action_frame())
	{
		m_TypeName = L"FIRE";
	}
	else if (m_sprite.is_endFrame())
		m_parent->Destroy(this);
	return true;
}

bool FX_Fire_Coal::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\FX\\Flame.png", 31, 56, 9, 9, 5,0,4);

	m_sprite = *State_Sprite[STATE_IDLE];

	return true;
}
