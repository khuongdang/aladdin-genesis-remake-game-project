#include "Math.h"



Math::Math()
{
}


Math::~Math()
{
}

float Math::abs(float number)
{
	if (number < 0.0)
		return -number;
	return number;
}

long Math::abs(long number)
{
	if (number < 0 )
		return -number;
	return number;
}

bool Math::isCollided(RECT a, RECT b)
{
	RECT rec_a = a;
	RECT rec_b = b;

	bool bot = rec_a.bottom >= rec_b.top && rec_a.bottom <= rec_b.bottom; // first object's bottom is in the middle of the second object
	bool top = rec_a.top >= rec_b.top && rec_a.top <= rec_b.bottom;
	bool right = rec_a.right <= rec_b.right && rec_a.right >= rec_b.left;
	bool left = rec_a.left <= rec_b.right && rec_a.left >= rec_b.left;

	bool first = (left && bot) || (left && top);
	bool second = (right && bot) || (right && top);

	bool third_one = left && (rec_a.bottom >= rec_b.bottom) && (rec_a.top <= rec_b.top);
	bool third_two = right && (rec_a.bottom >= rec_b.bottom) && (rec_a.top <= rec_b.top);
	bool third = third_one || third_two;

	bool fourth_one = top && (rec_a.left <= rec_b.left) && (rec_a.right >= rec_b.right);
	bool fourth_two = bot && (rec_a.left <= rec_b.left) && (rec_a.right >= rec_b.right);
	bool fourth = fourth_one || fourth_two;

	bool inside = (rec_a.top >= rec_b.top) && (rec_a.bottom <= rec_b.bottom) && (rec_a.left >= rec_b.left) && (rec_a.right <= rec_b.right);

	if (first || second || third || fourth || inside)
		return true;

	return false;
}
