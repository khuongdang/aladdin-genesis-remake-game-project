#pragma once
#include "System.h"
#include "HUD_Number.h"

class HUD_Lives
{
public:
	HUD_Lives();
	~HUD_Lives();

	bool Render(VEC2 vec, int amount);

	bool Render(float x, float y, int amount);
private:
	Sprite m_sprite = Sprite();
	HUD_Number* display;
	bool Init_Sprite();
};

