#include "Item_Monkey.h"
#include "Sound.h"
#include "GameScene.h"



Item_Monkey::Item_Monkey(VEC2 pos)
{
	m_TypeName = L"ITEM_MONKEY";

	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(45, 50);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Item_Monkey::~Item_Monkey()
{
}

bool Item_Monkey::Init_Sprite()
{
	State_Sprite.resize(1);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Item\\Monkey\\monkey.png", 40, 40, 10, 10, 5);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}

int Item_Monkey::Hit()
{
	Sound::GetInstance()->Play("Gem Collect");
	GameScene::getInstance()->Add_Score(300);
	if (m_parent != nullptr) m_parent->Destroy(this);
	return 0;
}
