#include "Item_Health.h"
#include "Sound.h"
#include "QuadTree.h"


Item_Health::Item_Health(VEC2 pos)
{
	m_TypeName = L"ITEM_HEALTH";
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(21, 26);	//temp
	m_rigid_body = Rect_Update();

	m_isStatic = true;
}


Item_Health::~Item_Health()
{
}

bool Item_Health::Init_Sprite()
{
	State_Sprite.resize(2);

	State_Sprite[STATE_IDLE] = new Sprite(L"Resource\\Sprite\\Item\\Health\\health.png", 21, 26, 4, 4,5);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}

int Item_Health::Hit()
{
	Sound::GetInstance()->Play("Extra Health");
	if (m_parent != nullptr) m_parent->Destroy(this);
	return 0;
}
