#include "Camel.h"
#include "GameScene.h"
#include "Saliva.h"
#include "Sound.h"



Camel::Camel(int X , int Y)
{
	m_TypeName = L"CAMEL";

	VEC2 pos = VEC2(X, Y);
	m_node = Node(pos);

	Init_Sprite();

	m_facing = FACE_RIGHT;
	m_state = STATE_IDLE;

	m_body_size = VEC2(72, 47);	//temp
	m_rigid_body = Rect_Update(); 

	m_isStatic = true;
}

bool Camel::Init_Sprite()
{
	State_Sprite.resize(2);

	State_Sprite[STATE_IDLE] = new Sprite( L"Resource\\Sprite\\Camel\\Idle.png", 72, 100,1,1);

	State_Sprite[STATE_STUNT] = new Sprite(L"Resource\\Sprite\\Camel\\Stunted.png", 200, 100, 7, 7, 4,0,4);

	m_sprite = *State_Sprite[STATE_IDLE];

	State_Sprite.shrink_to_fit();

	return true;
}

bool Camel::Update()
{	
	if (m_state == STATE_STUNT)
	{
		if (m_sprite.is_action_frame())
		{
			if (!m_actacked)
			{
				Saliva* pit = new Saliva(m_node.get_Position());
				pit->set_Velocity(VEC2(m_facing * 6, 0));
				GameScene::getInstance()->Add_Player_layer(pit);
				Sound::GetInstance()->Play("Camel_Spit");
				m_actacked = true;
			}				
		}

		if (m_sprite.is_startFrame())
		{
			m_actacked = false;
		}
		
		if (m_sprite.is_endFrame())
		{
			Change_State(STATE_IDLE);
		}
			
	}

	return true;
}

int Camel::Hit()
{
	Change_State(STATE_IDLE);
	Change_State(STATE_STUNT);
	return 0;
}

Camel::~Camel()
{
}
