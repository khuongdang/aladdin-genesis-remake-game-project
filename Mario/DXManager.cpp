#include "DXManager.h"


DXManager::DXManager() {};

DXManager::DXManager(HINSTANCE* hInstance, HWND* hwnd)
{
	m_hInstance = hInstance;
	m_hwnd = hwnd;
	m_Instance = *this;
}


DXManager::~DXManager()
{
}

bool DXManager::Init()
{
	m_d3d = Direct3DCreate9(D3D_SDK_VERSION);
	D3DPRESENT_PARAMETERS d3dpp;

	ZeroMemory(&d3dpp, sizeof(d3dpp));

	d3dpp.Windowed = TRUE; // FALSE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferHeight = SCREEN_HEIGHT;
	d3dpp.BackBufferWidth = SCREEN_WIDTH;

	m_d3d->CreateDevice(
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		*m_hwnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp,
		&m_d3ddv);

	if (m_d3ddv == NULL)
	{
		MessageBox(*m_hwnd, L"Can't not create DirectX Device", L"ERROR", MB_OK);
		return false;
	}

	m_d3ddv->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_back_buffer);

	// Create a small off-screen surface (will fill it contain in GameRun)
	int result =
		m_d3ddv->CreateOffscreenPlainSurface(
			100,					// width 				
			100,					// height
			D3DFMT_X8R8G8B8,		// format
			D3DPOOL_DEFAULT,		// where? (VRAM or RAM)
			&m_surface,
			NULL);

	GameScene(&m_d3ddv ,&m_back_buffer, &m_surface);

	if (GameScene::getInstance() == nullptr)
	{
		MessageBox(*m_hwnd, L"Can't not create Game Scene", L"ERROR", MB_OK);
		return false;
	}

	GameScene::getInstance()->Init();

	CutScene(&m_d3ddv, &m_back_buffer, &m_surface);

	if (CutScene::get_Instance() == nullptr)
	{
		MessageBox(*m_hwnd, L"Can't not create Cut Scene", L"ERROR", MB_OK);
		return false;
	}

	CutScene::get_Instance()->Init();

	return true;
}

bool DXManager::Run()
{
	if ( CutScene::get_Instance()->is_Active() )
	{
		CutScene::get_Instance()->Run();
	}
	else 
	{
		KeyboardManager::getInstance()->ProcessKeyboard();
		GameScene::getInstance()->Run();
	}
	return true;
}

bool DXManager::End()
{
	GameScene::getInstance()->End();
	return true;
}

DXManager* DXManager::getInstance()
{
	return &m_Instance;
}

LPDIRECT3DDEVICE9 * DXManager::getDevice()
{
	return &m_d3ddv;
}

HWND DXManager::getHWND()
{
	return *m_hwnd;
}

DXManager DXManager::m_Instance;


